function [tol, i, c, tollist, clist, a, b] = bisection(f, a, b, eps, N)
  
    delta = 1e-17;
    fa = f(a);
    fb = f(b);
    
    for i = 1 : N

        c = 0.5*(a+b);
        clist(1, i) = c;
        tol = 0.5*(b-a);
        tollist(1, i) = tol;
        
        if tol < eps
            break
        end
        
        fc = f(c);
        
        if abs(fc) < delta
            break 
        end
        
        if fa * fc < 0
            b = c;
            fb = fc;
        else
            a = c;
            fa = fc;
        end
    end
end




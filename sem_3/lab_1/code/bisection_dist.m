clc, clear, close all

f01 = @(x) 3.*x+log10(3.*x+2)-1;
a01 = 0;
b01 = 0.5;

f02 = @(x) x.^3-3.*x.^2-3.*x+11;
a02 = -2;
b02 = -1.5;

f03 = @(x)((x.^2+x-3)./(0.1*x))-1;
a03 = -2.5;
b03 = -2;

eps = 1e-15;
N = 1000;
n = 1;

[tol01, i01, c01, tollist01, clist01] = bisection(f01, a01, b01, eps, N);
[tol02, i02, c02, tollist02, clist02] = bisection(f02, a02, b02, eps, N);
[tol03, i03, c03, tollist03, clist03] = bisection(f03, a03, b03, eps, N);


for iter1 = 1 : 5
    
    a01 = 0;
    b01 = 0.5;
    releps1 = [];
    distlist1 = [];
    
    for j1 = 1 : N
        
        coef1 = 3+3.*(-iter1+(iter1+iter1).*rand(1,1))./100;
        f1 = @(x) coef1.*x+log10(3.*x+2)-1;
        
        if j1 == i01
            break
        end
        
        [tol1, i1, c1, tollist1, clist1, a1, b1] = bisection(f1, a01, b01, eps, n);
        releps1(1, j1) = abs((c1-clist01(1, j1))./clist01(1, j1));
        distlist1(1, j1) = abs((coef1 - 3)./3);
       
        a01 = a1;
        b01 = b1;
    end
    set(0,'DefaultAxesFontSize',12,'DefaultAxesFontName','Times New Roman');
    set(0,'DefaultTextFontSize',12,'DefaultTextFontName','Times New Roman');
    
    figure('Units', 'normalized', 'OuterPosition', [0 0 1 1]);
    plot(distlist1, releps1, 'MarkerFaceColor','red','Marker','o','MarkerEdgeColor','red','LineStyle','none')
    xlabel('disturb1 (возмущение)');
    ylabel('releps1 (относительная погрешность)');
    title('Зависимость относительной погрешности решения от возмущения исходных данных');
    legend('Половинное деление для f1');
    grid minor
end

for iter2 = 1 : 5
    
    a02 = -2;
    b02 = -1.5;
    releps2 = [];
    distlist2 = [];
    
    for j2 = 1 : N
        
        coef2 = -3-3.*(-iter2+(iter2+iter2).*rand(1,1))./100;
        f2 = @(x) x.^3+coef2.*x.^2-3.*x+11;
        
        if j2 == i02
            break
        end
        
        [tol2, i2, c2, tollist2, clist2, a2, b2] = bisection(f2, a02, b02, eps, n);
        releps2(1, j2) = abs((c2-clist02(1, j2))./clist02(1, j2));
        distlist2(1, j2) = abs((coef2 + 3)./(-3));
        
        a02 = a2;
        b02 = b2;
    end
    set(0,'DefaultAxesFontSize',12,'DefaultAxesFontName','Times New Roman');
    set(0,'DefaultTextFontSize',12,'DefaultTextFontName','Times New Roman');
    
    figure('Units', 'normalized', 'OuterPosition', [0 0 1 1]);
    plot(distlist2, releps2, 'MarkerFaceColor','green','Marker','o','MarkerEdgeColor','green','LineStyle','none')
    xlabel('disturb2 (возмущение)');
    ylabel('releps2 (относительная погрешность)');
    title('Зависимость относительной погрешности решения от возмущения исходных данных');
    legend('Половинное деление для f2');
    grid minor
end

for iter3 = 1 : 5
    
    a03 = -2.5;
    b03 = -2;
    releps3 = [];
    distlist3 = [];
    
    for j3 = 1 : N
        
        coef3 = 1+1.*(-iter3+(iter3+iter3).*rand(1,1))./100;
        f3 = @(x) ((coef3.*x.^2+x-3)./(0.1.*x))-1;
        
        if j3 == i03
            break
        end
        
        [tol3, i3, c3, tollist3, clist3, a3, b3] = bisection(f3, a03, b03, eps, n);
        releps3(1, j3) = abs((c3-clist03(1, j3))./clist03(1, j3));
        distlist3(1, j3) = abs(coef3 - 1);
        
        if tol3 < eps
            break
        end
        
        a03 = a3;
        b03 = b3;
    end
    set(0,'DefaultAxesFontSize',12,'DefaultAxesFontName','Times New Roman');
    set(0,'DefaultTextFontSize',12,'DefaultTextFontName','Times New Roman');
    
    figure('Units', 'normalized', 'OuterPosition', [0 0 1 1]);
    plot(distlist3, releps3, 'MarkerFaceColor','blue','Marker','o','MarkerEdgeColor','blue','LineStyle','none')
    xlabel('disturb3 (возмущение)');
    ylabel('releps3 (относительная погрешность)');
    title('Зависимость относительной погрешности решения от возмущения исходных данных');
    legend('Половинное деление для f3');
    grid minor
end

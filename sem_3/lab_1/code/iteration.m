function [tol, i, c, tollist, clist, x0] = iteration(fi, x0, eps, N) 
    
    c = fi(x0);
    
    for i = 1 : N
        
        tol = abs(c - x0);
        tollist(1, i) = tol;
        
        if tol < eps 
            break
        end
        
        x0 = c;
        c = fi(x0);
        clist(1, i) = c;
    
    end
    
end



clc, clear, close all

f01 = @(x) 3.*x+log10(3.*x+2)-1;
derf01 = @(x) 3+(3./(log(10).*(3.*x+2)));
alpha01 = derf01(0.5);
gamma01 = derf01(0);
lambda01 = 2./(alpha01+gamma01);
x01 = 0;
fi01 = @(x) x-lambda01.*(3.*x+log10(3.*x+2)-1);

f02 = @(x) x.^3-3.*x.^2-3.*x+11;
derf02 = @(x) 3.*x.^2-6.*x-3;
alpha02 = derf02(-1.5);
gamma02 = derf02(-2);
lambda02 = 2./(alpha02+gamma02);
x02 = -2;
fi02 = @(x) x-lambda02.*(x.^3-3.*x.^2-3.*x+11);

f03 = @(x)((x.^2+x-3)./(0.1*x))-1;
derf03 = @(x) 10+(30./(x.^2));
alpha03 = derf03(-2.5);
gamma03 = derf03(-2);
lambda03 = 2./(alpha03+gamma03);
x03 = -2.5;
fi03 = @(x) x-lambda03.*(((x.^2+x-3)./(0.1.*x))-1);

eps = 1e-15;
N = 1000;
n = 1;

[tol01, i01, c01, tollist01, clist01] = iteration(fi01, x01, eps, N);
[tol02, i02, c02, tollist02, clist02] = iteration(fi02, x02, eps, N);
[tol03, i03, c03, tollist03, clist03] = iteration(fi03, x03, eps, N);

for iter1 = 1 : 5
    
    x01 = 0;
    releps1 = [];
    distlist1 = [];
    
    for j1 = 1 : N
        
        coef1 = 3+3.*(-iter1+(iter1+iter1).*rand(1,1))./100;
        f1 = @(x) coef1.*x+log10(3.*x+2)-1;
        derf1 = @(x) coef1+(3./(log(10).*(3.*x+2)));
        alpha1 = derf1(0.5);
        gamma1 = derf1(0);
        lambda1 = 2./(alpha1+gamma1);
        fi1 = @(x) x-lambda1.*(coef1.*x+log10(3.*x+2)-1);
        
        if j1 == i01
            break
        end
        
        [tol1, i1, c1, tollist1, clist1, x1] = iteration(fi1, x01, eps, n);
        releps1(1, j1) = abs((c1-clist01(1, j1))./clist01(1, j1));
        distlist1(1, j1) = abs((coef1 - 3)./3);
        
        x01 = x1;
    end
    set(0,'DefaultAxesFontSize',12,'DefaultAxesFontName','Times New Roman');
    set(0,'DefaultTextFontSize',12,'DefaultTextFontName','Times New Roman');
    
    figure('Units', 'normalized', 'OuterPosition', [0 0 1 1]);
    plot(distlist1, releps1, 'MarkerFaceColor','red','Marker','o','MarkerEdgeColor','red','LineStyle','none')
    xlabel('disturb1 (возмущение)');
    ylabel('releps1 (относительная погрешность)');
    title('Зависимость относительной погрешности решения от возмущения исходных данных');
    legend('Простые итерации для f1');
    grid minor
end

for iter2 = 1 : 5
    
    x02 = -2;
    releps2 = [];
    distlist2 = [];
    
    for j2 = 1 : N
        
        coef2 = -3-3.*(-iter2+(iter2+iter2).*rand(1,1))./100;
        f2 = @(x) x.^3+coef2.*x.^2-3.*x+11;
        derf2 = @(x) 3.*x.^2+2.*coef2.*x-3;
        alpha2 = derf2(-1.5);
        gamma2 = derf2(-2);
        lambda2 = 2./(alpha2+gamma2);
        fi2 = @(x) x-lambda2.*(x.^3+coef2.*x.^2-3.*x+11);
        
        if j2 == i02
            break
        end
        
        [tol2, i2, c2, tollist2, clist2, x2] = iteration(fi2, x02, eps, n);
        releps2(1, j2) = abs((c2-clist02(1, j2))./clist02(1, j2));
        distlist2(1, j2) = abs((coef2 + 3)./(-3));
        
        x02 = x2;
    end
    set(0,'DefaultAxesFontSize',12,'DefaultAxesFontName','Times New Roman');
    set(0,'DefaultTextFontSize',12,'DefaultTextFontName','Times New Roman');
    
    figure('Units', 'normalized', 'OuterPosition', [0 0 1 1]);
    plot(distlist2, releps2, 'MarkerFaceColor','green','Marker','o','MarkerEdgeColor','green','LineStyle','none')
    xlabel('disturb2 (возмущение)');
    ylabel('releps2 (относительная погрешность)');
    title('Зависимость относительной погрешности решения от возмущения исходных данных');
    legend('Простые итерации для f2');
    grid minor
end

for iter3 = 1 : 5
    
    x03 = -2.5;
    releps3 = [];
    distlist3 = [];
    
    for j3 = 1 : N
        
        coef3 = 1+1.*(-iter3+(iter3+iter3).*rand(1,1))./100;
        f3 = @(x) ((x.^2+coef3.*x-3)./(0.1.*x))-1;
        derf3 = @(x) 10+(30./x.^2);
        alpha3 = derf3(-2.5);
        gamma3 = derf3(-2);
        lambda3 = 2./(alpha3+gamma3);
        fi3 = @(x) x-lambda3.*(((x.^2+coef3.*x-3)./(0.1.*x))-1);
        
        if j3 == i03
            break
        end
        
        [tol3, i3, c3, tollist3, clist3, x3] = iteration(fi3, x03, eps, n);
        releps3(1, j3) = abs((c3-clist03(1, j3))./clist03(1, j3));
        distlist3(1, j3) = abs(coef3 - 1);
        
        x03 = x3;
    end
    set(0,'DefaultAxesFontSize',12,'DefaultAxesFontName','Times New Roman');
    set(0,'DefaultTextFontSize',12,'DefaultTextFontName','Times New Roman');
    
    figure('Units', 'normalized', 'OuterPosition', [0 0 1 1]);
    plot(distlist3, releps3, 'MarkerFaceColor','blue','Marker','o','MarkerEdgeColor','blue','LineStyle','none')
    xlabel('disturb3 (возмущение)');
    ylabel('releps3 (относительная погрешность)');
    title('Зависимость относительной погрешности решения от возмущения исходных данных');
    legend('Простые итерации для f3');
    grid minor
end
    
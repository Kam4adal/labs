%% f_plots
clc, clear, close all;

set(0,'DefaultAxesFontSize',12,'DefaultAxesFontName','Times New Roman');
set(0,'DefaultTextFontSize',12,'DefaultTextFontName','Times New Roman');

% f1
f1 = @(x) 3.*x+log10(3.*x+2)-1;
t1 = -0.5:0.01:3;

figure('Units', 'normalized', 'OuterPosition', [0 0 1 1]);
plot(t1, f1(t1), 'LineWidth', 1.0);
xlabel('x');
ylabel('f1(x)');
title('График функции f1');
legend('f1(x) = 3*x+log10(3*x+2)-1');
grid minor;

% f2
f2 = @(x) x.^3-3.*x.^2-3.*x+11;
t2 = -3:0.01:4;

set(0,'DefaultAxesFontSize',12,'DefaultAxesFontName','Times New Roman');
set(0,'DefaultTextFontSize',12,'DefaultTextFontName','Times New Roman'); 
figure('Units', 'normalized', 'OuterPosition', [0 0 1 1]);
plot(t2, f2(t2), 'LineWidth', 1.0);
xlabel('x');
ylabel('f2(x)');
title('График функции f2');
legend('f2(x) = x^3-3*x^2-3*x+11');
grid minor;

% f3
f3 = @(x) ((x.^2+x-3)./(0.1.*x))-1;
t3 = -4:0.01:4;
 
figure('Units', 'normalized', 'OuterPosition', [0 0 1 1]);
plot(t3, f3(t3), 'LineWidth', 1.0);
xlabel('x');
ylabel('f3(x)');
ylim([-50 50]);
title('График функции f3');
legend('f3(x) = (x^2+x-3)/(0.1*x)-1');
grid minor

%% tol_vs_i
clc, clear, close all

% Исходные данные
f1 = @(x) 3.*x+log10(3.*x+2)-1;
derf1 = @(x) 3+(3./(log(10).*(3.*x+2)));
alpha1 = derf1(0.5);
gamma1 = derf1(0);
lambda1 = 2./(alpha1+gamma1);
a1 = -0.5;
b1 = 1;
x01 = 0;
fi1 = @(x) x-lambda1.*(3.*x+log10(3.*x+2)-1);

f2 = @(x) x.^3-3.*x.^2-3.*x+11;
derf2 = @(x) 3.*x.^2-6.*x-3;
alpha2 = derf2(-1.5);
gamma2 = derf2(-2);
lambda2 = 2./(alpha2+gamma2);
a2 = -2;
b2 = -1;
x02 = -2;
fi2 = @(x) x-lambda2.*(x.^3-3.*x.^2-3.*x+11);

f3 = @(x)((x.^2+x-3)./(0.1*x))-1;
derf3 = @(x) 10+(30./(x.^2));
alpha3 = derf3(-2.5);
gamma3 = derf3(-2);
lambda3 = 2./(alpha3+gamma3);
a3 = -2.5;
b3 = -2;
x03 = -2;
fi3 = @(x) x-lambda3.*(((x.^2+x-3)./(0.1.*x))-1);

eps = 1e-15;
N = 1000;

% Вычисления
[tol1, i1, c1, tollist1] = bisection(f1, a1, b1, eps, N);
[tol2, i2, c2, tollist2] = bisection(f2, a2, b2, eps, N);
[tol3, i3, c3, tollist3] = bisection(f3, a3, b3, eps, N);
[tl1, it1, r1, tllist1] = iteration(fi1, x01, eps, N);
[tl2, it2, r2, tllist2] = iteration(fi2, x02, eps, N);
[tl3, it3, r3, tllist3] = iteration(fi3, x03, eps, N);

iter1 = 1:i1;
iter2 = 1:i2;
iter3 = 1:i3;
iter4 = 1:it1;
iter5 = 1:it2;
iter6 = 1:it3;

% Построение графика
set(0,'DefaultAxesFontSize',12,'DefaultAxesFontName','Times New Roman');
set(0,'DefaultTextFontSize',12,'DefaultTextFontName','Times New Roman');
figure('Units', 'normalized', 'OuterPosition', [0 0 1 1]);
semilogy(iter1, tollist1, 'MarkerFaceColor','red','Marker','o','MarkerEdgeColor','red','LineStyle','none');
hold on
semilogy(iter2, tollist2, 'MarkerFaceColor','green','Marker','o','MarkerEdgeColor','green','LineStyle','none');
semilogy(iter3, tollist3, 'MarkerFaceColor','blue','Marker','o','MarkerEdgeColor','blue','LineStyle','none');
semilogy(iter4, tllist1, 'MarkerFaceColor','black','Marker','o','MarkerEdgeColor','black','LineStyle','none');
semilogy(iter5, tllist2, 'MarkerFaceColor','cyan','Marker','o','MarkerEdgeColor','cyan','LineStyle','none');
semilogy(iter6, tllist3, 'MarkerFaceColor','magenta','Marker','o','MarkerEdgeColor','magenta','LineStyle','none');
xlabel('i (номер итерации)');
ylabel('tol (погрешность)');
title('Зависимость погрешности от номера итерации');
legend('Половинное деление для f1', 'Половинное деление для f2', 'Половинное деление для f3', 'Простые итерации для f1', 'Простые итерации для f2', 'Простые итерации для f3');
grid minor

%% tol_vs_eps
clc, clear, close all

% Исходные данные
f1 = @(x) 3.*x+log10(3.*x+2)-1;
derf1 = @(x) 3+(3./(log(10).*(3.*x+2)));
alpha1 = derf1(0.5);
gamma1 = derf1(0);
lambda1 = 2./(alpha1+gamma1);
a1 = -0.5;
b1 = 2;
x01 = 0;
fi1 = @(x) x-lambda1.*(3.*x+log10(3.*x+2)-1);


f2 = @(x) x.^3-3.*x.^2-3.*x+11;
derf2 = @(x) 3.*x.^2-6.*x-3;
alpha2 = derf2(-1.5);
gamma2 = derf2(-2);
lambda2 = 2./(alpha2+gamma2);
a2 = -3;
b2 = -1.5;
x02 = -2;
fi2 = @(x) x-lambda2.*(x.^3-3.*x.^2-3.*x+11);

f3 = @(x)((x.^2+x-3)./(0.1*x))-1;
derf3 = @(x) 10+(30./(x.^2));
alpha3 = derf3(-2.5);
gamma3 = derf3(-2);
lambda3 = 2./(alpha3+gamma3);
a3 = -2.5;
b3 = -2;
x03 = -2;
fi3 = @(x) x-lambda3.*(((x.^2+x-3)./(0.1.*x))-1);

eps = 1e-15;
N = 1000;

% Вычисления
for iter = 1:N
    
    if eps > 0.5
        break
    end
  
    [tol1, i1, c1, tollist1] = bisection(f1, a1, b1, eps, N);
    [tol2, i2, c2, tollist2] = bisection(f2, a2, b2, eps, N);
    [tol3, i3, c3, tollist3] = bisection(f3, a3, b3, eps, N);
    [tl1, it1, r1, tllist1] = iteration(fi1, x01, eps, N);
    [tl2, it2, r2, tllist2] = iteration(fi2, x02, eps, N);
    [tl3, it3, r3, tllist3] = iteration(fi3, x03, eps, N);
    
    list1(1, iter) = tol1;
    list2(1, iter) = tol2;
    list3(1, iter) = tol3;
    list4(1, iter) = tl1;
    list5(1, iter) = tl2;
    list6(1, iter) = tl3;
    
    epslist(1, iter) = eps;
    eps = eps.*10;
    
end

% Построение графика
g = @(x) x;
t = 1e-15:1e-5:0.1;
set(0,'DefaultAxesFontSize',12,'DefaultAxesFontName','Times New Roman');
set(0,'DefaultTextFontSize',12,'DefaultTextFontName','Times New Roman'); 

figure('Units', 'normalized', 'OuterPosition', [0 0 1 1]);
loglog(epslist, list1, 'MarkerFaceColor','red','Marker','o','MarkerSize',6.0,'MarkerEdgeColor','red','LineStyle','none')
hold on
loglog(epslist, list2, 'MarkerFaceColor','green','Marker','o','MarkerSize',6.0,'MarkerEdgeColor','green','LineStyle','none')
loglog(epslist, list3, 'MarkerFaceColor','blue','Marker','o','MarkerSize',6.0,'MarkerEdgeColor','blue','LineStyle','none')
plot(t, g(t), 'LineStyle', '--', 'LineWidth', 1.0, 'Color', [0.6, 0.3, 0])
xlabel('eps (точность)');
ylabel('tol (погрешность)');
title('Зависимость погрешности от заданной точности');
legend('Половинное деление для f1', 'Половинное деление для f2', 'Половинное деление для f3', 'g = x');
grid minor

figure('Units', 'normalized', 'OuterPosition', [0 0 1 1]);
loglog(epslist, list4, 'MarkerFaceColor','black','Marker','o','MarkerSize',6.0,'MarkerEdgeColor','black','LineStyle','none')
hold on
loglog(epslist, list5, 'MarkerFaceColor','cyan','Marker','o','MarkerSize',6.0,'MarkerEdgeColor','cyan','LineStyle','none')
loglog(epslist, list6, 'MarkerFaceColor','magenta','Marker','o','MarkerSize',6.0,'MarkerEdgeColor','magenta','LineStyle','none')
plot(t, g(t), 'LineStyle', '--', 'LineWidth', 1.0, 'Color', [0.6, 0.3, 0])
xlabel('eps (точность)');
ylabel('tol (погрешность)');
title('Зависимость погрешности от заданной точности');
legend('Простые итерации для f1', 'Простые итерации для f2', 'Простые итерации для f3', 'g = x');
grid minor

%% N_vs_eps
clc, clear, close all

% Исходные данные
f1 = @(x) 3.*x+log10(3.*x+2)-1;
derf1 = @(x) 3+(3./(log(10).*(3.*x+2)));
alpha1 = derf1(0.5);
gamma1 = derf1(0);
lambda1 = 2./(alpha1+gamma1);
a1 = -0.5;
b1 = 1;
x01 = 0;
fi1 = @(x) x-lambda1.*(3.*x+log10(3.*x+2)-1);


f2 = @(x) x.^3-3.*x.^2-3.*x+11;
derf2 = @(x) 3.*x.^2-6.*x-3;
alpha2 = derf2(-1.5);
gamma2 = derf2(-2);
lambda2 = 2./(alpha2+gamma2);
a2 = -2;
b2 = -1.5;
x02 = -2;
fi2 = @(x) x-lambda2.*(x.^3-3.*x.^2-3.*x+11);

f3 = @(x)((x.^2+x-3)./(0.1*x))-1;
derf3 = @(x) 10+(30./(x.^2));
alpha3 = derf3(-2.5);
gamma3 = derf3(-2);
lambda3 = 2./(alpha3+gamma3);
a3 = -3;
b3 = -2;
x03 = -2;
fi3 = @(x) x-lambda3.*(((x.^2+x-3)./(0.1.*x))-1);

eps = 1e-15;
N = 1000;

% Вычисления
for iter = 1:N
    
    if eps > 0.5
        break
    end
    
    [tol1, i1, c1, tollist1] = bisection(f1, a1, b1, eps, N);
    [tol2, i2, c2, tollist2] = bisection(f2, a2, b2, eps, N);
    [tol3, i3, c3, tollist3] = bisection(f3, a3, b3, eps, N);
    [tl1, it1, r1, tllist1] = iteration(fi1, x01, eps, N);
    [tl2, it2, r2, tllist2] = iteration(fi2, x02, eps, N);
    [tl3, it3, r3, tllist3] = iteration(fi3, x03, eps, N);
    
    Nlist1(1, iter) = i1;
    Nlist2(1, iter) = i2;
    Nlist3(1, iter) = i3;
    Nlist4(1, iter) = it1;
    Nlist5(1, iter) = it2;
    Nlist6(1, iter) = it3;
    
    epslist(1, iter) = eps;
    eps = eps*10;
    
end

% Построение графика
set(0,'DefaultAxesFontSize',12,'DefaultAxesFontName','Times New Roman');
set(0,'DefaultTextFontSize',12,'DefaultTextFontName','Times New Roman'); 
figure('Units', 'normalized', 'OuterPosition', [0 0 1 1]);
semilogx(epslist, Nlist1, 'MarkerFaceColor','red','Marker','o','MarkerEdgeColor','red','LineStyle','none');
hold on
semilogx(epslist, Nlist2, 'MarkerFaceColor','green','Marker','o','MarkerEdgeColor','green','LineStyle','none');
semilogx(epslist, Nlist3, 'MarkerFaceColor','blue','Marker','o','MarkerEdgeColor','blue','LineStyle','none');
semilogx(epslist, Nlist4, 'MarkerFaceColor','magenta','Marker','o','MarkerEdgeColor','magenta','LineStyle','none');
semilogx(epslist, Nlist5, 'MarkerFaceColor','cyan','Marker','o','MarkerEdgeColor','cyan','LineStyle','none');
semilogx(epslist, Nlist6, 'MarkerFaceColor','black','Marker','o','MarkerEdgeColor','black','LineStyle','none');
xlabel('eps (точность)');
ylabel('N (использованное число вычислений)');
title('Зависимость использованного числа вычислений функции от заданной точности');
legend('Половинное деление для f1', 'Половинное деление для f2', 'Половинное деление для f3', 'Простые итерации для f1', 'Простые итерации для f2', 'Простые итерации для f3');
grid minor

%% f1(test)
clc, clear, close all;

f1 = @(x) 3*x+log10(3*x+2)-1;
x01 = [-0.5 1];
options = optimset('Display','iter'); 
[x fval exitflag output] = fzero(f1,x01,options)

%% f2(test)
clc, clear, close all;

% fzero
f2 = @(x) x.^3-3.*x.^2-3.*x+11;
x02 = 1;
options = optimset('Display','iter'); 
[x fval exitflag output] = fzero(f2,x02,options)

% roots
p = [1 -3 -3 11];
r = roots(p)

%% f3(test)
clc, clear, close all;

f3 = @(x)((x.^2+x-3)./(0.1*x))-1;
x03 = [-6 6];
options = optimset('Display','iter'); 
[x fval exitflag output] = fzero(f3,x03,options)


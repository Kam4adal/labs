function [A, B, C, D, n] = Coef(X, Y)

    n = length(X)-1;
    H = zeros(1, n);
    
    for e = 1:n
        H(e) = X(e+1)-X(e);
    end
    
    Fs = zeros(1, n);
    
    for i = 1:n
        fs = (Y(i+1)-Y(i))./H(i);
        Fs(i) = fs;
    end
    
    Del = zeros(1, n-1);
    L = zeros(1, n-1);
    
    delta = -H(2)./(2.*H(1) + 2.*H(2));
    Del(1) = delta;
    
    lambda = (3.*Fs(2)-3.*Fs(1))./(2.*H(1)+2.*H(2));
    L(1) = lambda;
    
    for j = 2:n-1
        lambda = (3.*Fs(j+1)-3.*Fs(j)-H(j).*lambda)./(2.*H(j)+2.*H(j+1)+H(j).*delta);
        L(j) = lambda;
        
        delta = -H(j+1)./(2.*H(j)+2.*H(j+1)+H(j).*delta);
        Del(j) = delta;
    end
    
    C = zeros(1, n+1);
    
    for k1 = n:-1:2
        C(k1) = Del(k1-1).*C(k1+1)+L(k1-1);
    end
    
    A = zeros(1, n);
    
    for k0 = 2:n+1
        A(k0-1) = Y(k0);
    end
    
    B = zeros(1, n);
    D = zeros(1, n);
    
    for k2 = 1:n
        B(k2) = Fs(k2)+(2./3).*H(k2).*C(k2+1)+(1./3).*H(k2).*C(k2);
        D(k2) = (C(k2+1)-C(k2))./(3.*H(k2));
    end
    
end

function [t, y] = Construct(x, X, k3, t, A, B, C, D)
    
    y = [];
    
    for k = t:length(x)
        
        if x(k) <= X(k3)
            
            yi = A(k3-1) + B(k3-1).*(x(k)-X(k3)) + C(k3).*(x(k)-X(k3)).^2 + D(k3-1).*(x(k)-X(k3)).^3;
            y = [y yi];
            t = t + 1;
            
        end
        
    end
    
end
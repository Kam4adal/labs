%% 
clc, clear, close all

f1 = @(x) sin(x);
f2 = @(x) cos(x);

a = 0;
b = pi;
x = a:0.0001*pi:b;

y1 = f1(x);
y2 = f2(x);

N = 100;

E1 = [];
E2 = [];
H = [];

for n = 5:N
    
    X = linspace(a, b, n);
    Y1 = f1(X);
    Y2 = f2(X);
    
    h = (b-a)./n;
    H = [H h];
    
    pp1 = csape(X, Y1, 'not-a-knot');
    pp2 = csape(X, Y2, 'not-a-knot');
    
    yy1 = fnval(pp1, x);
    yy2 = fnval(pp2, x);
    
    e1 = max(abs(yy1 - y1));
    e2 = max(abs(yy2 - y2));
    
    E1 = [E1 e1];
    E2 = [E2 e2];
  
   
end

figure
semilogy(H, E1, 'LineWidth', 1.0)
hold on
semilogy(H, E2, 'LineWidth', 1.0)
xlabel('h');
ylabel('E');
title('График зависимости максимальной ошибки от шага сетки');
legend('Максимальная ошибка для sin(x)', 'Максимальная ошибка для cos(x)', 'Location', 'Southeast');
grid minor

%% 
clc, clear, close all

f1 = @(x) x + cos(x);
f2 = @(x) 3.*sign(x).*x.^4-8.*x.^3-18.*x.^2+2;

X = linspace(-2, 2, 10);
Y1 = f1(X);
Y2 = f2(X);

x = -2:0.0003:2;

pp1 = csape(X, Y1, 'variational');
pp2 = csape(X, Y2, 'variational');

yy1 = fnval(pp1, x);
yy2 = fnval(pp2, x);

figure
plot(x, yy1, 'LineWidth', 1.0)
hold on
plot(X, Y1, 'ro')
xlabel('x');
ylabel('y1');
title('Построение кубического сплайна(variational) с помощью csape');
legend('Сплайн', 'Функция f1', 'Location', 'Southeast');
grid minor

figure
plot(x, yy2, 'LineWidth', 1.0)
hold on
plot(X, Y2, 'ro')
xlabel('x');
ylabel('y2');
title('Построение кубического сплайна(variational) с помощью csape');
legend('Сплайн', 'Функция f2');
grid minor

%% 
clc, clear, close all

f1 = @(x) x + cos(x);
f2 = @(x) 3.*sign(x).*x.^4-8.*x.^3-18.*x.^2+2;

X = linspace(-2, 2, 10);
Y1 = f1(X);
Y2 = f2(X);

x = -2:0.0003:2;

pp1 = csape(X, Y1, 'periodic');
pp2 = csape(X, Y2, 'periodic');

yy1 = fnval(pp1, x);
yy2 = fnval(pp2, x);

figure
plot(x, yy1, 'LineWidth', 1.0)
hold on
plot(X, Y1, 'ro')
xlabel('x');
ylabel('y1');
title('Построение кубического сплайна(periodic) с помощью csape');
legend('Сплайн', 'Функция f1', 'Location', 'Southeast');
grid minor

figure
plot(x, yy2, 'LineWidth', 1.0)
hold on
plot(X, Y2, 'ro')
xlabel('x');
ylabel('y2');
title('Построение кубического сплайна(periodic) с помощью csape');
legend('Сплайн', 'Функция f2');
grid minor

%% 
clc, clear, close all

f1 = @(x) x + cos(x);
f2 = @(x) 3.*sign(x).*x.^4-8.*x.^3-18.*x.^2+2;

X = linspace(-2, 2, 10);
Y1 = f1(X);
Y2 = f2(X);

x = -2:0.0003:2;

pp1 = csape(X, Y1, 'not-a-knot');
pp2 = csape(X, Y2, 'not-a-knot');

yy1 = fnval(pp1, x);
yy2 = fnval(pp2, x);

figure
plot(x, yy1, 'LineWidth', 1.0)
hold on
plot(X, Y1, 'ro')
xlabel('x');
ylabel('y1');
title('Построение кубического сплайна(not-a-knot) с помощью csape');
legend('Сплайн', 'Функция f1', 'Location', 'Southeast');
grid minor

figure
plot(x, yy2, 'LineWidth', 1.0)
hold on
plot(X, Y2, 'ro')
xlabel('x');
ylabel('y2');
title('Построение кубического сплайна(not-a-knot) с помощью csape');
legend('Сплайн', 'Функция f2');
grid minor

%% 
clc, clear, close all

f1 = @(x) x + cos(x);
f2 = @(x) 3.*sign(x).*x.^4-8.*x.^3-18.*x.^2+2;

X = linspace(-2, 2, 10);
Y1 = f1(X);
Y2 = f2(X);

x = -2:0.0003:2;

pp1 = csape(X, [10 Y1 12], [1 2]);
pp2 = csape(X, [15 Y2 24], [2 1]);

yy1 = fnval(pp1, x);
yy2 = fnval(pp2, x);

figure
plot(x, yy1, 'LineWidth', 1.0)
hold on
plot(X, Y1, 'ro')
xlabel('x');
ylabel('y1');
title('Построение кубического сплайна(custom) с помощью csape');
legend('Сплайн', 'Функция f1', 'Location', 'Southeast');
grid minor

figure
plot(x, yy2, 'LineWidth', 1.0)
hold on
plot(X, Y2, 'ro')
xlabel('x');
ylabel('y2');
title('Построение кубического сплайна(custom) с помощью csape');
legend('Сплайн', 'Функция f2');
grid minor
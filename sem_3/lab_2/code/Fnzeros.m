%% 
clc, clear, close all

spline = spmak(1:31,rand(1,25)-0.5); 
x = fnzeros(fnder(spline));
y = fnval(spline,x);
figure
fnplt(spline);
hold on
plot(x,y,'ro')
xlabel('x');
ylabel('y');
title('Нахождение корней сплайна');
legend('Сплайн', 'Точки экстремума сплайна');
grid minor

%% 
clc, clear, close all

spline = spmak([1 1 2 3 5 8],[13 -21 34 -55]);
x1 = fnzeros(spline);
x2 = fnzeros(fnder(spline));
y1 = fnval(spline,x1);
y2 = fnval(spline,x2);

figure
fnplt(spline);
hold on
plot(x1,y1,'ro')
xlabel('x');
ylabel('y');
title('Нахождение корней сплайна');
legend('Сплайн', 'Корни сплайна');
grid minor

figure
fnplt(spline);
hold on
plot(x2,y2,'ro')
xlabel('x');
ylabel('y');
title('Нахождение точек экстремума сплайна');
legend('Сплайн', 'Точки экстремума');
grid minor

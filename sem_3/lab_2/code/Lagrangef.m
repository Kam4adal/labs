function y = Lagrangef(x, X, Y)

n = length(X);
y = 0;

for i = 1:n
    
    P = 1;
   
    for j = 1:n
        
        if j ~= i
            P = P.*(x-X(j))/(X(i)-X(j));
        end
        
    end
    
    y = y + Y(i).*P;
    
end
function yy = Main(x, X, Y)

    [A, B, C, D, n] = Coef(X, Y);
    
    t = 1;
    yy = [];

    for k3 = 2:n+1
        [t, y] = Construct(x, X, k3, t, A, B, C, D);
        yy = [yy y];
    end
    
end
%% 2.Иллюстрация работы сплайнового метода
clc, clear, close all

f1 = @(x) x + cos(x);
f2 = @(x) 3.*sign(x).*x.^4-8.*x.^3-18.*x.^2+2;

a = -2;
b = 2;
x = a:0.0003:b;

Nmax = 10;

y1 = f1(x);
y2 = f2(x);

Em1 = [];
Em2 = [];

for N = 6:2:Nmax
    
    E1 = [];
    E2 = [];
    
    X = linspace(a, b, N);
    Y1 = f1(X);
    Y2 = f2(X);
    
    yy1 = Main(x, X, Y1);
    yy2 = Main(x, X, Y2);
   
    e1 = abs(yy1 - y1);
    e2 = abs(yy2 - y2);
   
    E1 = [E1 e1];
    E2 = [E2 e2];
    
    em1 = max(abs(yy1 - y1));
    em2 = max(abs(yy2 - y2));
    
    Em1 = [Em1 em1];
    Em2 = [Em2 em2];
    
    figure
    plot(x, yy1, 'LineWidth', 1.0);
    hold on
    plot(X, Y1, 'ro');
    xlabel('x');
    ylabel('y1');
    title('Иллюстрация работы сплайнового метода');
    legend('Полином для f1', 'Функция f1');
    grid minor
    
    figure
    plot(x, yy2, 'LineWidth', 1.0);
    hold on
    plot(X, Y2, 'ro');
    xlabel('x');
    ylabel('y2');
    title('Иллюстрация работы сплайнового метода');
    legend('Полином для f2', 'Функция f2');
    grid minor
    
    figure
    plot(x, E1, 'LineWidth', 1.0);
    xlabel('x');
    ylabel('E1');
    title('График поточечной ошибки для f1');
    legend('Поточечная ошибка для f1');
    grid minor
    
    figure
    plot(x, E2, 'LineWidth', 1.0);
    xlabel('x');
    ylabel('E2');
    title('График поточечной ошибки для f2');
    legend('Поточечная ошибка для f2');
    grid minor
    
end

figure
semilogy(6:2:N, Em1, 'LineWidth', 1.0)
xlabel('N');
ylabel('Em1');
title('График максимальной ошибки f1');
legend('Максимальная ошибка для f1');
grid minor

figure
semilogy(6:2:N, Em2, 'LineWidth', 1.0)
xlabel('N');
ylabel('Em2');
title('График максимальной ошибки f2');
legend('Максимальная ошибка для f2');
grid minor

%% 3.Зависимость ошибки интерполяции от степени интерполяционного полинома
clc, clear, close all

f1 = @(x) x + cos(x);
f2 = @(x) 3.*sign(x).*x.^4-8.*x.^3-18.*x.^2+2;

a = -2;
b = 2;
x = a:0.0003:b;

Nmax = 100;

y1 = f1(x);
y2 = f2(x);

Em1 = [];
Em2 = [];

for N = 6:2:Nmax
    
    X = linspace(a, b, N);
    
    Y1 = f1(X);
    Y2 = f2(X);
    
    yy1 = Main(x, X, Y1);
    yy2 = Main(x, X, Y2);
    
    em1 = max(abs(yy1 - y1));
    em2 = max(abs(yy2 - y2));
    
    Em1 = [Em1 em1];
    Em2 = [Em2 em2];
    
end

figure
semilogy(6:2:N, Em1, 'LineWidth', 1.0)
xlabel('N');
ylabel('Em1');
title('График максимальной ошибки f1');
legend('Максимальная ошибка для f1');
grid minor    

figure
semilogy(6:2:N, Em2, 'LineWidth', 1.0)
xlabel('N');
ylabel('Em2');
title('График максимальной ошибки f2');
legend('Максимальная ошибка для f2');
grid minor  

%% 4.Зависимость ошибки в выбранных точках от степени интерполяционного полинома
clc, clear, close all

f1 = @(x) x + cos(x);
f2 = @(x) 3.*sign(x).*x.^4-8.*x.^3-18.*x.^2+2;

a = -2;
b = 2;
x = a:0.0003:b;

px1 = x(15);
px2 = x(517);

py11 = f1(px1);
py12 = f1(px2);
py21 = f2(px1);
py22 = f2(px2);

Em11 = [];
Em12 = [];
Em21 = [];
Em22 = [];

Nmax = 100;

for N = 6:2:Nmax
    
    X = linspace(a, b, N);
    Y1 = f1(X);
    Y2 = f2(X);
    
    yy11 = Main(px1, X, Y1);
    yy12 = Main(px2, X, Y1);
    yy21 = Main(px1, X, Y2);
    yy22 = Main(px2, X, Y2);
    
    em11 = abs(yy11 - py11);
    em12 = abs(yy12 - py12);
    em21 = abs(yy21 - py21);
    em22 = abs(yy22 - py22);
    
    Em11 = [Em11 em11];
    Em12 = [Em12 em12];
    Em21 = [Em21 em21];
    Em22 = [Em22 em22];
    
end

figure
semilogy(6:2:N, Em11, 'LineWidth', 1.0)
hold on
semilogy(6:2:N, Em12, 'LineWidth', 1.0)
xlabel('N');
ylabel('Em');
title('График максимальной ошибки для выбранных точек f1');
legend('Максимальная ошибка для точки px1', 'Максимальная ошибка для точки px2');
grid minor    

figure
semilogy(6:2:N, Em21, 'LineWidth', 1.0)
hold on
semilogy(6:2:N, Em22, 'LineWidth', 1.0)
xlabel('N');
ylabel('Em');
title('График максимальной ошибки для выбранных точек f2');
legend('Максимальная ошибка для точки px1', 'Максимальная ошибка для точки px2');
grid minor   
    
%% 5.Зависимость ошибки интерполяционного полинома при возмущении данных
clc, clear, close all

f1 = @(x) x + cos(x);
f2 = @(x) 3.*sign(x).*x.^4-8.*x.^3-18.*x.^2+2;

a = -2;
b = 2;
N = 100;

X = linspace(a, b, N);

Y01 = f1(X);
Y02 = f2(X);

x = a:0.0003:b;

y1 = f1(x);
y2 = f2(x);

yy01 = Main(x, X, Y01);
yy02 = Main(x, X, Y02);

e01 = max(abs(yy01-y1));
e02 = max(abs(yy02-y2));

dist1 = zeros(1, 5);
dist2 = zeros(1, 5);

E1 = zeros(1, 5);
E2 = zeros(1, 5);

for j = 1:5
    
    for i = 1:length(X)
        Y1(i) = Y01(i)+Y01(i).*(-j+(j+j).*rand()).*0.01;
        Y2(i) = Y02(i)+Y02(i).*(-j+(j+j).*rand()).*0.01;
    end
    
    yy1 = Main(x, X, Y1);
    yy2 = Main(x, X, Y2);
    
    dist1i = abs((Y1-Y01)./Y01);
    dist2i = abs((Y2-Y02)./Y02);
 
    dist1(j) = max(dist1i);
    dist2(j) = max(dist2i);
    
    e1 = max(abs(yy1 - y1));
    e2 = max(abs(yy2 - y2));
    
    erel1 = (e1-e01)./e01;
    erel2 = (e2-e02)./e02;
    
    E1(j) = erel1;
    E2(j) = erel2;
   
end

figure
plot(dist1, E1, 'MarkerFaceColor','blue','Marker','o','MarkerEdgeColor','blue','LineStyle','none')
xlabel('dist1');
ylabel('E1');
title('График зависимости максимальной ошибки от возмущения f1');
legend('Ошибка от возмущения f1');
grid minor

figure
plot(dist2, E2, 'MarkerFaceColor','red','Marker','o','MarkerEdgeColor','red','LineStyle','none')
xlabel('N');
ylabel('Em');
title('График зависимости максимальной ошибки от возмущения f2');
legend('Ошибка от возмущения f2');
grid minor

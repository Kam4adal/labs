%% 
clc, clear, close all

f1 = @(x) x + cos(x);

X = linspace(-2, 2, 10);
Y1 = f1(X);

splinetool(X, Y1)

%% 
clc, clear, close all

f2 = @(x) 3.*sign(x).*x.^4-8.*x.^3-18.*x.^2+2;

X = linspace(-2, 2, 10);
Y2 = f2(X);

splinetool(X, Y2)


%% Cетка со сгущением
clc, clear, close all

f1 = @(x) x + cos(x);
f2 = @(x) 3.*sign(x).*x.^4-8.*x.^3-18.*x.^2+2;

a1 = -3;
b1 = 2;
p1 = -1.5;
eps1 = 0.5;

a2 = -1;
b2 = 3;
p2 = 1.8;
eps2 = 0.5;

x1 = a1:0.0001:b1;
x2 = a2:0.0003:b2;

y1 = f1(x1);
y2 = f2(x2);

Em1 = [];
Em2 = [];

N1list = [];
N2list = [];

Nmax = 100;

for N = 6:2:Nmax
    
    [X1, N1] = addnodesf(a1, b1, N, p1, eps1);
    [X2, N2] = addnodesf(a2, b2, N, p2, eps2);
    
    N1list = [N1list N1];
    N2list = [N2list N2];
    
    Y1 = f1(X1);
    Y2 = f2(X2);
    
    yy1 = Splinef(x1, X1, Y1);
    yy2 = Splinef(x2, X2, Y2);
    
    em1 = max(abs(yy1 - y1));
    em2 = max(abs(yy2 - y2));
    
    Em1 = [Em1 em1];
    Em2 = [Em2 em2];
   
end

figure
semilogy(N1list, Em1)
xlabel('N');
ylabel('Em1');
title('График максимальной ошибки f1(сетка со сгущением)');
legend('Максимальная ошибка для f1');
grid minor    

figure
semilogy(N2list, Em2)
xlim([100, 250])
xlabel('N');
ylabel('Em2');
title('График максимальной ошибки f2(сетка со сгущением)');
legend('Максимальная ошибка для f2');
grid minor  

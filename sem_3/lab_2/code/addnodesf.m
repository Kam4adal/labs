function [X, N] = addnodesf(a, b, N, p, eps)
   
   n = N-1;
   
   pn = p-eps;
   pp = p+eps;
  
   h1 = (pn-a)/n;
   h2 = (b-pp)/n;
   
   hh = 0.5*eps;
   
   x1 = pn;
   xad1 = [];
   
   H = [];
   
   while hh > 1e-15
       H = [H hh];
       x1 = x1 + hh;
       xad1 = [xad1 x1];
       hh = 0.5*hh;
   end
   
   x2 = pp;
   xad2 = [];
   
   for i = 1:length(H)
       x2 = x2 - H(i);
       xad2 = [xad2 x2];
   end
   
   xad2 = fliplr(xad2);
   
   X = [a:h1:pn, xad1, p, xad2, pp:h2:b];
   
   N = length(X);

end
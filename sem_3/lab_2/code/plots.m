%% plots
clc, clear, close all
f1 = @(x) x + cos(x);
f2 = @(x) 3.*sign(x).*x.^4-8.*x.^3-18.*x.^2+2;
t = -3:0.01:2;
p = -1:0.01:3;

figure
plot(t, f1(t), 'LineWidth', 1.0);
xlabel('x');
ylabel('f1');
title('График f1');
legend('Функция f1');
grid minor

figure
plot(p, f2(p), 'LineWidth', 1.0);
xlabel('x');
ylabel('f2');
title('График f2');
legend('Функция f2');
grid minor

function [x, A] = Gaussf(A, b)

n = length(b);

for k = 1:n-1
    
    [~, p] = max(abs(A(k:n, k)));
    p = p + k - 1;
    
    if p ~= k
        A([k,p], :) = A([p,k], :);
        b([k,p]) = b([p,k]);
    end
    
    for i = k+1:n
        m = A(i, k)/A(k, k);
        A(i, k:n) = A(i, k:n) - m*A(k, k:n);
        b(i) = b(i) - m*b(k);
    end
end

x = zeros(n, 1);
x(n) = b(n)/A(n, n);

for k = n-1:-1:1
    x(k) = (b(k) - A(k, k+1:n)*x(k+1:n))/A(k, k);
end

end
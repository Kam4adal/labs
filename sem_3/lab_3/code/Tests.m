%% Зависимость точности от числа обусловленности
clc, clear, close all

Acc = [];
Cond = [];

for i = 1:5
    
    A = rand(10);
    [u, d, v] = svd(A);
    d = eye(10);
    d(1, 1) = 10^i;
    A = u*d*v';
    cond(A)
    xt = ones(10, 1);
    b = A*xt;
    
    x = Gaussf(A, b);
    
    c = cond(A);
    Cond = [Cond c];
    
    acc = norm(x - xt);
    Acc = [Acc acc];
   
end

loglog(Cond, Acc)
xlabel('cond');
ylabel('acc');
title('График зависимости точности от числа обусловленности');
legend('acc(cond)');
grid minor

%% Зависимость времени выполнения от числа обусловленности
clc, clear, close all

Time = [];
Cond = [];

for i = 1:5
    
    A = rand(10);
    [u, d, v] = svd(A);
    d = eye(10);
    d(1, 1) = 10^i;
    A = u*d*v';
    
    c = cond(A);
    Cond = [Cond c];
    
    xt = ones(10, 1);
    b = A*xt;
    
    tic
    x = Gaussf(A, b);
    time = toc;
   
    Time = [Time time];
   
end

loglog(Cond, Time)
xlabel('cond');
ylabel('time');
title('График зависимости времени выполнения от числа обусловленности');
legend('time(cond)');
grid minor

%% Зависимость относительной погрешности от возмущения правой части
clc, clear, close all

A = rand(10);
[u, d, v] = svd(A);
d = eye(10);
d(1, 1) = 2;
A = u*d*v';

xt = ones(10, 1);
b = A*xt;
n = length(b);
bn = zeros(n, 1);

Dist = zeros(1, 3);
Rel = zeros(1, 3);

for i = 1:3
    
    bn(1:n) = b(1:n)+b(1:n).*(-i+(i+i).*rand()).*0.01;
    
    dist = max(abs((bn-b)./b));
    Dist(i) = dist;
    
    x = Gaussf(A, bn);
    
    rel = max(abs(x-xt)./xt);
    Rel(i) = rel;
    
end

plot(Dist, Rel, 'o')
xlabel('dist');
ylabel('rel');
title('График зависимости относительной погрешности от возмущения правой части');
legend('rel(dist)');
grid minor

%% Зависимость относительной погрешности от возмущения наименьшего элемента матрицы
clc, clear, close all

A = rand(10);
[u, d, v] = svd(A);
d = eye(10);
d(1, 1) = 2;
A = u*d*v';
n = length(A);

xt = ones(10, 1);
b = A*xt;

Dist = zeros(1, 3);
Rel = zeros(1, 3);

m = min(min(A));

for i = 1:3
    
   
    for j = 1:n
        for k = 1:n
            if(A(j, k) == m)
                a = A(j, k);
                break
            end
        end
    end
    
    A(j, k) = a+a.*(-i+(i+i).*rand()).*0.01;
    dist = abs((A(j, k)-a)./a);
    Dist(i) = dist;
    
    x = Gaussf(A, b);
    
    rel = max(abs(x-xt)./xt);
    Rel(i) = rel;
    
end

plot(Dist, Rel, 'o')
xlabel('dist');
ylabel('rel');
title('График зависимости относительной погрешности от возмущения наибольшего элемента матрицы');
legend('rel(dist)');
grid minor

function [x, tol, k, Tol] = Richardsonf(A, b, x0, eps, n)
    
    L = eig(A);
    lmax = max(L);
    lmin = min(L);
    
    ksi = lmin/lmax;
    ro = (1-ksi)/(1+ksi);
    tau0 = 2/(lmin+lmax);
    
    t = cos(pi/(2*n));
    tau = tau0/(1+ro*t);
    
    x = x0 + tau.*(-A*x0 + b);
    
    Tol = [];
    
    for k = 2:n
        
        tol = norm(x-x0);
        Tol = [Tol tol];
        
        if tol < eps
            break
        end
        
        t = cos(pi*(2*k-1)/(2*n));
        tau = tau0/(1+ro*t);
        
        x0 = x;
        x = x0 + tau.*(-A*x0 + b);
        
    end

end
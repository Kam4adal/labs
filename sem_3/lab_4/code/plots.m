%% Зависимость точности от числа обусловленности
clc, clear, close all

eps = 1e-6;
n = 1e5;

Acc = [];
Cond = [];

for i = 1:100
    
    A = rand(10);
    [u, d, v] = svd(A);
    d = eye(10);
    d(1, 1) = i;
    A = u*d*v';
    A = A*A';
    
    xt = ones(10, 1);
    b = A*xt;
    
    x0 = rand(size(b));
    x = Richardsonf(A, b, x0, eps, n);
    
    c = cond(A);
    Cond = [Cond c];
    
    acc = norm(x - xt, inf);
    Acc = [Acc acc];
   
end

loglog(Cond, Acc)
xlabel('cond');
ylabel('acc');
title('График зависимости точности от числа обусловленности');
legend('acc(cond)');
grid minor

%% Зависимость времени выполнения от числа обусловленности
clc, clear, close all

eps = 1e-6;
n = 1e5;

Time = [];
Cond = [];

for i = 1:100
    
    A = rand(10);
    [u, d, v] = svd(A);
    d = eye(10);
    d(1, 1) = i;
    A = u*d*v';
    A = A*A';
    
    xt = ones(10, 1);
    b = A*xt;
    
    x0 = rand(size(b));
   
    tic
    x = Richardsonf(A, b, x0, eps, n);
    time = toc;
   
    Time = [Time time];
    
    c = cond(A);
    Cond = [Cond c];
    
end

loglog(Cond, Time)
xlabel('cond');
ylabel('time');
title('График зависимости времени выполнения от числа обусловленности');
legend('time(cond)');
grid minor

%% Зависимость относительной погрешности от возмущения правой части
clc, clear, close all

A = rand(10);
[u, d, v] = svd(A);
d = eye(10);
d(1, 1) = 2;
A = u*d*v';
A = A*A';

xt = ones(10, 1);
b = A*xt;

x0 = rand(size(b));
eps = 1e-6;
n = 1e5;

p = length(b);
bn = zeros(p, 1);

Dist = zeros(1, 3);
Rel = zeros(1, 3);

for i = 1:3
    
    bn(1:p) = b(1:p)+b(1:p).*(-i+(i+i).*rand()).*0.01;
   
    dist = norm((bn-b)./b);
    Dist(i) = dist;
    
    x = Richardsonf(A, bn, x0, eps, n);
    
    rel = norm((x-xt)./xt);
    Rel(i) = rel;
    
end

plot(Dist, Rel)
xlabel('dist');
ylabel('rel');
title('График зависимости относительной погрешности от возмущения правой части');
legend('rel(dist)');
grid minor

%% Зависимость относительной погрешности от возмущения наибольшего элемента матрицы
clc, clear, close all

A = rand(10);
[u, d, v] = svd(A);
d = eye(10);
d(1, 1) = 10;
A = u*d*v';
A = A*A';

xt = ones(10, 1);
b = A*xt;

x0 = rand(size(b));
eps = 1e-6;
n = 1e5;

Dist = zeros(1, 3);
Rel = zeros(1, 3);

m = min(A(:));
j = find(A==m);
a = A(j);

for i = 1:3
    
    A(j) = a+a.*(-i+(i+i).*rand()).*0.01;
    dist = norm((A(j)-a)./a);
    Dist(i) = dist;
    
    x = Richardsonf(A, b, x0, eps, n);
    
    rel = norm((x-xt)./xt);
    Rel(i) = rel;
    
end

plot(Dist, Rel)
xlabel('dist');
ylabel('rel');
title('График зависимости относительной погрешности от возмущения наибольшего элемента матрицы');
legend('rel(dist)');
grid minor

%% Зависимость погрешности от заданной точности
clc, clear, close all

A = rand(10);
[u, d, v] = svd(A);
d = eye(10);
d(1, 1) = 10;
A = u*d*v';
A = A*A';

xt = ones(10, 1);
b = A*xt;

x0 = rand(size(b));
n = 1e5;
eps = 1e-15;

Tol = zeros(1, 13);
Eps = zeros(1, 13);

for i = 1:13
    
    [x, tol] = Richardsonf(A, b, x0, eps, n);
    Tol(i) = tol;
    Eps(i) = eps;
    eps = eps*10;
    
end

loglog(Eps, Tol, 'o')
xlabel('eps');
ylabel('tol');
title('Зависимость погрешности от заданной точности');
legend('tol(eps)');
grid minor

%% Зависимость числа итераций от заданной точности
clc, clear, close all

A = rand(10);
[u, d, v] = svd(A);
d = eye(10);
d(1, 1) = 10;
A = u*d*v';
A = A*A';

xt = ones(10, 1);
b = A*xt;

x0 = rand(size(b));
n = 1e5;
eps = 1e-15;

K = zeros(1, 13);
Eps = zeros(1, 13);

for i = 1:13
    
    [x, tol, k] = Richardsonf(A, b, x0, eps, n);
    K(i) = k;
    Eps(i) = eps;
    eps = eps*10;
    
end

loglog(Eps, K, 'o')
xlabel('eps');
ylabel('K');
title('Зависимость числа итераций от заданной точности');
legend('K(eps)');
grid minor

%% Уменьшение погрешности с ходом итераций
clc, clear, close all

A = rand(10);
[u, d, v] = svd(A);
d = eye(10);
d(1, 1) = 10;
A = u*d*v';
A = A*A';

xt = ones(10, 1);
b = A*xt;

x0 = rand(size(b));
n = 1e5;
eps = 1e-6;

[x, tol, k, Tol] = Richardsonf(A, b, x0, eps, n);
I = 1:k-1;

semilogy(I, Tol)
xlabel('i');
ylabel('tol');
title('Уменьшение погрешности с ходом итераций');
legend('tol(i)');
grid minor

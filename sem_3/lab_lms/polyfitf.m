clc, clear, close all

f = @(x) 3.*x.^4-8.*x.^3-18.*x.^2+2;

x = linspace(-2, 3, 100);
y = f(x)+17*randn(size(x));

xx = linspace(-2, 3, 1000);
yy = f(xx);

for k = 1:5
    
    p = polyfit(x, y, k);
    yp = polyval(p, xx);
    
    if k == 4
        yx = polyval(p, x);
        e = y-yx;
    end
    
    subplot(6, 1, k)
    plot(x, y, '.', xx, yp)
    title('График приближения')
    xlabel('x')
    ylabel('y')
    legend('Данные', 'Приближение')
    grid minor
    
end

subplot(6, 1, 6)
plot(x, e, '.', x, zeros(size(x)))
title('График ошибки')
xlabel('x')
ylabel('e')
legend('Ошибка приближения k=4')
grid minor

clc, clear, close all

A1 = [1 2 3; 4 5 6; 7 8 9];
det1 = det(A1);

A2 = [1e8 2e8 3e8; 4e8 5e8 6e8; 7e8 8e8 9e8];
det2 = det(A2);

B = rand(3, 1);

x1 = A1\B;
x2 = A2\B;
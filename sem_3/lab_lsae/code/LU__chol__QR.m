%% LU
clc, clear, close all

N = 300;
time = zeros(1, 296);

for n = 5:N
    
    A = rand(n);
    B = ones(n, 1);
    
    tic
    [L, U] = lu(A);
    y = L\B;
    x = U\y;
    time(n-4) = toc;
    
end

figure
loglog(5:N, time)
xlabel('n')
ylabel('time')
title('Зависимость временных затрат на решение СЛАУ от размера матрицы(lu)')
legend('time(n)', 'Location', 'Southeast')
grid minor

%% chol
% clc, clear, close all

N = 300;
time = zeros(1, 296);

for n = 5:N
    
    A = rand(n);
    [u, d, v] = svd(A);
    d = eye(n);
    As = u*d*v';
    As = As*As';
    B = ones(n, 1);
    
    tic
    R = chol(As);
    y = R'\B;
    x = R\y;
    time(n-4) = toc;
    
end

figure
loglog(5:N, time)
xlabel('n')
ylabel('time')
title('Зависимость временных затрат на решение СЛАУ от размера матрицы(chol)')
legend('time(n)', 'Location', 'Southeast')
grid minor

%% QR
% clc, clear, close all

N = 300;
time = zeros(1, 296);

for n = 5:N
    
    A = rand(n);
    B = ones(n, 1);
    
    tic
    [Q, R] = qr(A);
    y = Q'*B;
    x = R\y;
    time(n-4) = toc;
    
end

figure
loglog(5:N, time)
xlabel('n')
ylabel('time')
title('Зависимость временных затрат на решение СЛАУ от размера матрицы(qr)')
legend('time(n)', 'Location', 'Southeast')
grid minor

%% UT
clc, clear, close all

L = tril(rand(1e4));
B = ones(1e4, 1);

opts.LT = true;

tic
x1 = L\B;
t1 = toc;

tic
x2 = linsolve(L, B, opts);
t2 = toc;

speedup = t1/t2

%% POSDEF
clc, clear, close all

A = rand(50);
B = ones(50, 1);

SP = A+A'+40*eye(50);

opts.SYM = true;
opts.POSDEF = true;

tic
x1 = SP\B;
t1 = toc;

tic
x2 = linsolve(SP, B, opts);
t2 = toc;

speedup = t1/t2

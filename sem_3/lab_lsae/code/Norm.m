clc, clear, close all

A1 = [10; 15; 20];
sum = 0;

for i = 1:length(A1)
    sum = sum + A1(i).^2;
end

n = sqrt(sum);
norm(A1);

A2 = [1 2 3; 4 5 6; 7 8 9];
B2 = A2'*A2;
e2 = eig(B2);
n2 = max(sqrt(e2));
norm(A2);

A3 = hilb(5);
B3 = A3'*A3;
e3 = eig(B3);
n3 = max(sqrt(e3));
norm(A3);

clc, clear, close all

N = 5:700;

xx1 = [];
xxq = [];

for n = N(1):N(end)

    A = -ones(n);
    A = tril(A, -1) + eye(n);
    A(:, end) = 1;

    xt = rand(n, 1);
    b = A*xt;

    x = A\b;
    xx1 = [xx1 norm(x-xt)/norm(xt)];
    
    [Q, R] = qr(A);
    z = Q'*b;
    x = R\z;
    xxq = [xxq norm(x-xt)/norm(xt)];
   
end

figure 
semilogy(N, xx1)
xlabel('n');
ylabel('xx1')
legend('xx1(n)')
title('Зависимость ошибки от размера матрицы для метода "\"')
grid minor

figure
semilogy(N, xxq)
xlabel('n');
ylabel('xxq')
legend('xxq(n)')
title('Зависимость ошибки от размера матрицы для QR-разложения')
grid minor

function [x, err, i, Err, flag] = Conjgrad(A, x, b, N, eps, P)
   
    flag = 0;
    i = 0;
    Err = [];
    bnrm = norm(b);
    
    if bnrm == 0
        bnrm = 1;
    end
    
    r = b - A*x;
    err = norm(r)/bnrm;
    
    if err < eps
        return
    end
    
    for i = 1:N
        
        z = LUsolver(P, r);
        rho = r' * z;
        
        if i > 1
            beta = rho / rho1;
            p = z + beta * p;
        else
            p = z;
        end
        
        q = A * p;
        alpha = rho/(p'*q);
        x = x + alpha*p;
        r = r - alpha*q;
        err = norm(r)/bnrm;
        Err = [Err err];
        
        if err <= eps
            break
        end
        
        rho1 = rho;
        
    end
    
    if err > eps 
        flag = 1;
    end
    
end

function x = LUsolver(A, b)

n = size(A, 1);

y = zeros(n, 1);
y(1) = b(1);

    for i = 2:n
        s1 = A(i, 1:i-1)*y(1:i-1);
        y(i) = b(i) - s1;
    end

x = zeros(n, 1);
x(n) =  y(n)/A(n, n);

    for j = n:-1:2
        s2 = A(j-1, n:-1:j)*x(n:-1:j);
        x(j-1) = (y(j-1) - s2)/A(j-1, j-1);
    end
    
end
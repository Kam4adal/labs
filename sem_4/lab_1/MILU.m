function A = MILU(A)

    n = size(A, 1);
    
    for r = 1:n-1
        d = 1/A(r, r);
        for i = r+1:n
            if A(i, r) ~= 0
                e = A(i, r)*d;
            	A(i, r) = e;
                for j = (r+1):n
                    if A(r, j) ~= 0 
                        if A(i, j) ~= 0
                            A(i, j) = A(i, j)-e*A(r, j);
                        else
                            A(i, i) = A(i, i)-e*A(r, j);
                        end
                    end
                end
            end
        end
    end
end
                   
    
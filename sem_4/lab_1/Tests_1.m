%% Зависимость точности от размера матрицы
clc, clear, close all

N = 10^3;
eps = 1e-15;

Err = [];
Size = [];

for k = 10:53
    
    G = numgrid('B', k);
    A = delsq(G);
    P = MILU(A);
    s = size(A, 1);
    x0 = zeros(s, 1);
    
    xt = ones(s, 1);
    b = A*xt;
    
    [~, err] = Conjgrad(A, x0, b, N, eps, P);
    
    Err = [Err err];
    Size = [Size s];
    
end

semilogy(Size, Err)
xlabel('Size');
ylabel('Err');
title('График зависимости точности от размера матрицы');
legend('err(size)');
grid minor

%% Зависимость времени выполнения от размера матрицы
clc, clear, close all

N = 10^3;
eps = 1e-15;

Time = [];
Size = [];

for n = 10:53
    
    G = numgrid('B', n);
    A = delsq(G);

    s = size(A, 1);
    x0 = zeros(s, 1);
    
    xt = ones(s, 1);
    b = A*xt;
    
    t = cputime;
    P = MILU(A);
    [~] = Conjgrad(A, x0, b, N, eps, P);
    time = cputime - t;
   
    Time = [Time time];
    Size = [Size s];
    
end

plot(Size, Time)
xlabel('Size');
ylabel('Time');
title('График зависимости времени выполнения от размера матрицы');
legend('time(size)');
grid minor

%% Зависимость относительной погрешности от возмущения правой части
clc, clear, close all

G = numgrid('B', 47);
A = delsq(G);
P = MILU(A);

s = size(A, 1);
x0 = rand(s, 1);

xt = ones(s, 1);
b = A*xt;

bn = zeros(s, 1);

N = 10^3;
eps = 1e-15;

Dist = zeros(1, 3);
Rel = zeros(1, 3);

for k = 1:3
    
    for i = 1:s
        bn(i) = b(i)+b(i).*(-k+(k+k).*rand()).*0.01;
    end
    
    dist = max(abs((bn-b)./b));
    Dist(k) = dist;
    
    x = Conjgrad(A, x0, bn, N, eps, P);
 
    rel = max(abs((x-xt)./xt));
    Rel(k) = rel;
    
end

plot(Dist, Rel, 'o')
xlabel('dist');
ylabel('rel');
title('График зависимости относительной погрешности от возмущения правой части');
legend('rel(dist)');
grid minor

%% Зависимость погрешности от заданной точности
clc, clear, close all

G = numgrid('B', 47);
A = delsq(G);
P = MILU(A);

s = size(A, 1);
x0 = rand(s, 1);
    
xt = ones(s, 1);
b = A*xt;

N = 10^3;
eps = 1e-15;

Err = zeros(1, 13);
Eps = zeros(1, 13);

for k = 1:13
    
    [~, err] = Conjgrad(A, x0, b, N, eps, P);
    Err(k) = err;
    Eps(k) = eps;
    eps = eps*10;
    
end

loglog(Eps, Err)
xlabel('eps');
ylabel('err');
title('Зависимость погрешности от заданной точности');
legend('err(eps)');
grid minor

%% Зависимость числа итераций от заданной точности
clc, clear, close all

G = numgrid('B', 47);
A = delsq(G);
P = MILU(A);

s = size(A, 1);
x0 = rand(s, 1);
    
xt = ones(s, 1);
b = A*xt;

N = 10^3;
eps = 1e-15;

K = zeros(1, 13);
Eps = zeros(1, 13);

for k = 1:13
    
    [~, ~, i] = Conjgrad(A, x0, b, N, eps, P);
    K(k) = i;
    Eps(k) = eps;
    eps = eps*10;
    
end

loglog(Eps, K)
xlabel('eps');
ylabel('K');
title('Зависимость числа итераций от заданной точности');
legend('K(eps)');
grid minor

%% Уменьшение погрешности с ходом итераций
clc, clear, close all

G = numgrid('B', 47);
A = delsq(G);
P = MILU(A);

s = size(A, 1);
x0 = rand(s, 1);
    
xt = ones(s, 1);
b = A*xt;

N = 10^3;
eps = 1e-15;

[~, ~, i, Err] = Conjgrad(A, x0, b, N, eps, P);
I = 1:i;

semilogy(I, Err)
xlabel('i');
ylabel('err');
title('Уменьшение погрешности с ходом итераций');
legend('err(i)');
grid minor

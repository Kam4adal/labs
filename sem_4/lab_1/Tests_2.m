clc, clear, close all

k = 75;
n = 3000;

D = zeros(1, k);

T = zeros(1, k);
N = zeros(1, k);

Trcm = zeros(1, k);
Nrcm = zeros(1, k);

Tamd = zeros(1, k);
Namd = zeros(1, k);

for i = 1:k
    
    density = i/n;
    D(i) = density;
    A = sprandsym(n, density) + 100*speye(n);
    
    t = cputime;
    L = chol(A);
    T(i) = cputime - t;
    N(i) = nnz(L);
    
    trcm = cputime;
    p1 = symrcm(A);
    Lrcm = chol(A(p1, p1));
    Trcm(i) = cputime - trcm;
    Nrcm(i) = nnz(Lrcm);
    
    tamd = cputime;
    p2 = symamd(A);
    Lamd = chol(A(p2, p2));
    Tamd(i) = cputime - tamd;
    Namd(i) = nnz(Lamd);
    
end

figure 
plot(D, N, D, Nrcm, D, Namd)
xlabel('density');
ylabel('Nz');
title('Графики заполнения множителей разложения Холецкого');
legend('Исходная  матрица', 'Обратный алгоритм Катхилла-Макки', 'Алгоритм наименьшей степени');
grid minor

figure
plot(D, T, D, Trcm, D, Tamd)
xlabel('density');
ylabel('Time');
title('Графики временных затрат на разложение Холецкого');
legend('Исходная  матрица', 'Обратный алгоритм Катхилла-Макки', 'Алгоритм наименьшей степени');
grid minor

%% spy

n = 2000;
density = 2/n;
A = sprandsym(n, density) + 100*speye(n);

L = chol(A);
figure
spy(A)

figure
spy(L)

p1 = symrcm(A);
Lrcm = chol(A(p1, p1));

figure
spy(A(p1, p1));

figure
spy(Lrcm)

p2 = symamd(A);
Lamd = chol(A(p2, p2));

figure
spy(A(p2, p2));

figure
spy(Lamd)
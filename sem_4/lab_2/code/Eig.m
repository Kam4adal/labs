function [V, D] = Eig(A, N, eps)

A = Givens_hess(A);
n = size(A, 1);

V = zeros(n);
D = zeros(n, 1);

[B, C] = RootSeparation(A);

for i = 1:n
    d = bisection(A, B(i), C(i), eps, N);
    
    M = A-d*eye(n);
    v = Gauss_hess(M);
    
    V(:, i) = v/norm(v);
    D(i) = d;
end

end
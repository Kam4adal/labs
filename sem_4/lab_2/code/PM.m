function [d1, y, errd, i] = PM(A, y0, N, delta, eps)

    n = size(A, 1);
    d0 = ones(n, 1);
    
    for i = 1:N
        
        y = A*y0;
        d = y./y0;
       
        if min(abs(y0)) < delta
            break
        end
        
        errd = norm(d-d0);
        
        if errd < eps
            break
        end
        
        d0 = d;
        y0 = y;
        
    end
    
    d1 = sum(d)/n;
    
end
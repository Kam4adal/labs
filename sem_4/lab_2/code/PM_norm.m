function [d1, x0, errd, i] = PM_norm(A, y0, N, delta, eps)

    n = size(A, 1);
    d0 = ones(n, 1);
    x0 = y0./norm(y0);
    
    for i = 1:N
        
        y = A*x0;
        d = y./x0;
        x0 = y./norm(y);
        
        if min(abs(x0)) < delta
            break
        end
        
        errd = norm(d-d0);
        
        if errd < eps
            break
        end
        
        d0 = d;
        
    end
    
    d1 = sum(d)./n;
    
end
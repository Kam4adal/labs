function y = PolyVal(P, x)

n = length(P);
y = 0;

for i = 1:n
    f = @(z) P(i)*z^(11-i);
    y = y + f(x);
end

end
%% Зависимость погрешности определения СЧ и СВ от отделимости
clc, clear, close all

N = 1e3;
delta = 1e-6;
eps = 1e-12;
y0 = ones(10, 1);

Mu = [];
Errd1 = [];
Errd2 = [];
Errx1 = [];
Errx2 = [];
Errd3 = [];
    
for n = 9:-0.5:1
    
    A = rand(10);
    [Q, R] = qr(A);
    D = linspace(10, n, 10);
    R = R-diag(diag(R))+diag(D);
    A = Q*R*Q';
    
    [V1, L1] = eig(A);
    m1 = max(L1(:));
    [r1, c1] = find(L1 == m1);
    
    Ah = hess(A);
    [Vh, Lh] = eig(Ah);
    L2 = sort(diag(Lh), 'descend');
    V2 = zeros(10);
    for i = 1:10
        [r2, c2] = find(Lh == L2(i));
        V2(:, i) = Vh(:, c2);
    end
    V2 = abs(V2);
    
    mu = D(1)/D(2);
    
    Mu = [Mu mu];
    
    [d1, x, errd1] = PM_norm(A, y0, N, delta, eps);
    errx1 = abs(sin(acos((V1(:, c1)'*x)/(norm(V1(:, c1))*norm(x)))));
    [d2, errd2] = LR(A, N, eps);
    [V, d3] = Eig(A, N, eps);
    errd3 = norm(D'-d3);
    V = abs(V);
    
    for i = 1:10
        errx = abs(sin(acos((V(:, i)'*V2(:, i))/(norm(V(:, i))*norm(V2(:, i))))));
        Err(i) = errx;
    end
    
    errx2 = max(Err);
    
    Errd1 = [Errd1 errd1];
    Errx1 = [Errx1 errx1];
    Errx2 = [Errx2 errx2];
    Errd2 = [Errd2 errd2];
    Errd3 = [Errd3 errd3];

end

figure
semilogy(Mu(1:5), Errd1(1:5), 'red', Mu(1:5), Errd2(1:5), 'green');
xlabel('Mu');
ylabel('Errd');
title('График зависимости погрешности определения СЧ от отделимости');
legend('PMnorm', 'LR');
grid minor

figure
semilogy(Mu, Errd3, 'blue');
xlabel('Mu');
ylabel('Errd');
title('График зависимости погрешности определения СЧ от отделимости');
legend('Eig');
grid minor

figure
semilogy(Mu(1:5), Errx1(1:5));
xlabel('Mu');
ylabel('ErrX');
title('График зависимости погрешности определения СВ от отделимости');
legend('PMnorm');
grid minor

figure
semilogy(Mu, Errx2);
xlabel('Mu');
ylabel('ErrV');
title('График зависимости погрешности определения СВ от отделимости');
legend('Eig');
grid minor

%% Зависимость погрешности от возмущения в матрице
clc, clear, close all

N = 1e3;
delta = 1e-6;
eps = 1e-12;
y0 = ones(10, 1);

A = rand(10);
[Q, R] = qr(A);
D1 = linspace(10, 1, 10);
D2 = linspace(10, 9, 10);
R1 = R-diag(diag(R))+diag(D1);
R2 = R-diag(diag(R))+diag(D2);
A1 = Q*R1*Q';
B1 = A1;
A2 = Q*R2*Q';
B2 = A2;

[V11, L11] = eig(A1);
m11 = max(L11(:));
[r11, c11] = find(L11 == m11);

Ah1 = hess(A1);
[Vh1, Lh1] = eig(Ah1);
L21 = sort(diag(Lh1), 'descend');
V21 = zeros(10);
for i = 1:10
    [r21, c21] = find(Lh1 == L21(i));
    V21(:, i) = Vh1(:, c21);
end
V21 = abs(V21);

[V12, L12] = eig(A2);
m12 = max(L12(:));
[r12, c12] = find(L12 == m12);

Ah2 = hess(A2);
[Vh2, Lh2] = eig(Ah2);
L22 = sort(diag(Lh2), 'descend');
V22 = zeros(10);
for i = 1:10
    [r22, c22] = find(Lh2 == L22(i));
    V22(:, i) = Vh2(:, c22);
end
V22 = abs(V22);

Reld11 = zeros(1, 5);
Reld12 = zeros(1, 5);
Reld13 = zeros(1, 5);
Relx1 = zeros(1, 5);
Relv1 = zeros(1, 5);

Reld21 = zeros(1, 5);
Reld22 = zeros(1, 5);
Reld23 = zeros(1, 5);
Relx2 = zeros(1, 5);
Relv2 = zeros(1, 5);

Dist1 = zeros(1, 10);
Dist11 = zeros(1, 5);
Dist2 = zeros(1, 10);
Dist22 = zeros(1, 5);

for k = 1:5
    
    for i = 1:10
        B1(1, i) = A1(1, i)+A1(1, i)*(-k+(k+k)*rand())*0.01;
        dist1 = abs((B1(1, i)-A1(1, i))/A1(1, i));
        Dist1(i) = dist1;
    end
    
    dist11 = max(Dist1);
    Dist11(k) = dist11;
    
    for i = 1:10
        B2(1, i) = A2(1, i)+A2(1, i)*(-k+(k+k)*rand())*0.01;
        dist2 = abs((B2(1, i)-A2(1, i))/A2(1, i));
        Dist2(i) = dist2;
    end
    
    dist22 = max(Dist2);
    Dist22(k) = dist22;
    
    [d11, x1, errd11] = PM_norm(B1, y0, N, delta, eps);
    errx1 = abs(sin(acos((V11(:, c11)'*x1)/(norm(V11(:, c11))*norm(x1)))));
    [d12, errd12] = LR(B1, N, eps);
    [V13, d13] = Eig(B1, N, eps);
    V13 = abs(V13);
    Err1 = zeros(1, 10);
    for i = 1:10
        err1 = abs(sin(acos((V13(:, i)'*V21(:, i))/(norm(V13(:, i))*norm(V21(:, i))))));
        Err1(i) = err1;
    end
    
    errv1 = max(Err1);
    
    [d21, x2] = PM_norm(B2, y0, N, delta, eps);
    errx2 = abs(sin(acos((V12(:, c12)'*x2)/(norm(V12(:, c12))*norm(x2)))));
    d22 = LR(B2, N, eps);
    [V23, d23] = Eig(B2, N, eps);
    V23 = abs(V23);
    Err2 = zeros(1, 10);
    for i = 1:10
        err2 = abs(sin(acos((V23(:, i)'*V22(:, i))/(norm(V23(:, i))*norm(V22(:, i))))));
        Err2(i) = err2;
    end
    
    errv2 = max(Err2);
    
    reld11 = norm(d11-D1(1))/norm(D1(1));
    relx1 = errx1/norm(V11(:, c11));
    relv1 = errv1/norm(V21);
    reld12 = norm(d12-D1')/norm(D1);
    reld13 = norm(d13-D1')/norm(D1);
    
    reld21 = norm(d21-D2(1))/norm(D2(1));
    relx2 = errx2/norm(V12(:, c12));
    relv2 = errv2/norm(V22);
    reld22 = norm(d22-D2')/norm(D2);
    reld23 = norm(d23-D2')/norm(D2);
    
    Reld11(k) = reld11;
    Reld12(k) = reld12;
    Reld13(k) = reld13;
    Relx1(k) = relx1;
    Relv1(k) = relv1;
   
    Reld21(k) = reld21;
    Reld22(k) = reld22;
    Reld23(k) = reld23;
    Relx2(k) = relx2;
    Relv2(k) = relv2;
end

figure
plot(Dist11, Reld11, 'o');
xlabel('Dist');
ylabel('Reld');
title('График зависимости погрешности от возмущения в матрице с хорошей отделимостью');
legend('PMnorm');
grid minor

figure
plot(Dist11, Relx1, 'o');
xlabel('Dist');
ylabel('Relx');
title('График зависимости погрешности СВ от возмущения в матрице с хорошей отделимостью');
legend('PMnorm');
grid minor

figure
plot(Dist11, Relv1, 'o');
xlabel('Dist');
ylabel('Relv');
title('График зависимости погрешности СВ от возмущения в матрице с хорошей отделимостью');
legend('Eig');
grid minor
figure
plot(Dist22, Reld21, 'o');
xlabel('Dist');
ylabel('Reld');
title('График зависимости погрешности от возмущения в матрице с плохой отделимостью');
legend('PMnorm');
grid minor

figure
plot(Dist22, Relx2, 'o');
xlabel('Dist');
ylabel('Relx');
title('График зависимости погрешности СВ от возмущения в матрице с плохой отделимостью');
legend('PMnorm');
grid minor

figure
plot(Dist22, Relv2, 'o');
xlabel('Dist');
ylabel('Relv');
title('График зависимости погрешности СВ от возмущения в матрице с плохой отделимостью');
legend('Eig');
grid minor

figure
plot(Dist11, Reld12, 'o');
xlabel('Dist');
ylabel('Reld');
title('График зависимости погрешности от возмущения в матрице с хорошей отделимостью');
legend('LR');
grid minor

figure
plot(Dist22, Reld22, 'o');
xlabel('Dist');
ylabel('Reld');
title('График зависимости погрешности от возмущения в матрице с плохой отделимостью');
legend('LR');
grid minor

figure
plot(Dist11, Reld13, 'o');
xlabel('Dist');
ylabel('Reld');
title('График зависимости погрешности от возмущения в матрице с хорошей отделимостью');
legend('Eig');
grid minor

figure
plot(Dist22, Reld23, 'o');
xlabel('Dist');
ylabel('Reld');
title('График зависимости погрешности от возмущения в матрице с плохой отделимостью');
legend('Eig');
grid minor

%% Симметричные и несимметричные матрицы с хорошо и плохо отделимыми СЧ
clc, clear, close all

N = 1e3;
delta = 1e-6;
eps = 1e-12;
y0 = ones(10, 1);

A = rand(10);
A1 = sprandsym(A);

[Q, R] = qr(A);
[u, d, v] = svd(full(A1));

D1 = linspace(10, 2, 10);
D2 = linspace(10, 8, 10);

R1 = R-diag(diag(R))+diag(D1);
R2 = R-diag(diag(R))+diag(D2);

d1 = sqrt(diag(D1));
d2 = sqrt(diag(D2));

A1 = Q*R1*Q';
A2 = Q*R2*Q';
A3 = u*d1*v';
A3 = A3*A3';
A4 = u*d2*v';
A4 = A4*A4';

[V11, L11] = eig(A1);
[V12, L12] = eig(A2);
[V13, L13] = eig(A3);
[V14, L14] = eig(A4);

m11 = max(L11(:));
[r11, c11] = find(L11 == m11);
m12 = max(L12(:));
[r12, c12] = find(L12 == m12);
m13 = max(L13(:));
[r13, c13] = find(L13 == m13);
m14 = max(L14(:));
[r14, c14] = find(L14 == m14);

Ah21 = hess(A1);
[Vh21, Lh21] = eig(Ah21);
L21 = sort(diag(Lh21), 'descend');
V21 = zeros(10);
for i = 1:10
    [r21, c21] = find(Lh21 == L21(i));
    V21(:, i) = Vh21(:, c21);
end
V21 = abs(V21);

Ah22 = hess(A2);
[Vh22, Lh22] = eig(Ah22);
L22 = sort(diag(Lh22), 'descend');
V22 = zeros(10);
for i = 1:10
    [r22, c22] = find(Lh22 == L22(i));
    V22(:, i) = Vh22(:, c22);
end
V22 = abs(V22);

Ah23 = hess(A3);
[Vh23, Lh23] = eig(Ah23);
L23 = sort(diag(Lh23), 'descend');
V23 = zeros(10);
for i = 1:10
    [r23, c23] = find(Lh23 == L23(i));
    V23(:, i) = Vh23(:, c23);
end
V23 = abs(V23);

Ah24 = hess(A4);
[Vh24, Lh24] = eig(Ah24);
L24 = sort(diag(Lh24), 'descend');
V24 = zeros(10);
for i = 1:10
    [r24, c24] = find(Lh24 == L24(i));
    V24(:, i) = Vh24(:, c24);
end
V24 = abs(V24);

[d11, x1, errd11] = PM_norm(A1, y0, N, delta, eps);
[d12, x2, errd12] = PM_norm(A2, y0, N, delta, eps);
[d13, x3, errd13] = PM_norm(A3, y0, N, delta, eps);
[d14, x4, errd14] = PM_norm(A4, y0, N, delta, eps);

errx1 = abs(sin(acos((V11(:, c11)'*x1)/(norm(V11(:, c11))*norm(x1)))));
errx2 = abs(sin(acos((V12(:, c12)'*x2)/(norm(V12(:, c12))*norm(x2)))));
errx3 = abs(sin(acos((V13(:, c13)'*x3)/(norm(V13(:, c13))*norm(x3)))));
errx4 = abs(sin(acos((V14(:, c14)'*x4)/(norm(V14(:, c14))*norm(x4)))));

[d21, errd21] = LR(A1, N, eps);
[d22, errd22] = LR(A2, N, eps);
[d23, errd23] = LR(A3, N, eps);
[d24, errd24] = LR(A4, N, eps);

[V31, d31] = Eig(A1, N, eps);
errd31 = norm(D1'-d31);
V31 = abs(V31);
Err1 = zeros(1, 10);
for i = 1:10
    err1 = abs(sin(acos((V31(:, i)'*V21(:, i))/(norm(V31(:, i))*norm(V21(:, i))))));
    Err1(i) = err1;
end

errv1 = max(Err1);

[V32, d32] = Eig(A2, N, eps);
errd32 = norm(D2'-d32);
V32 = abs(V32);
Err2 = zeros(1, 10);
for i = 1:10
    err2 = abs(sin(acos((V32(:, i)'*V22(:, i))/(norm(V32(:, i))*norm(V22(:, i))))));
    Err2(i) = err2;
end

errv2 = max(Err2);

[V33, d33] = Eig(A3, N, eps);
errd33 = norm(D1'-d33);
V33 = abs(V33);
Err3= zeros(1, 10);
for i = 1:10
    err3 = abs(sin(acos((V33(:, i)'*V23(:, i))/(norm(V33(:, i))*norm(V23(:, i))))));
    Err3(i) = err3;
end

errv3 = max(Err3);

[V34, d34] = Eig(A4, N, eps);
errd34 = norm(D2'-d34);
V34 = abs(V34);
Err4 = zeros(1, 10);
for i = 1:10
    err4 = abs(sin(acos((V34(:, i)'*V24(:, i))/(norm(V34(:, i))*norm(V24(:, i))))));
    Err4(i) = err4;
end

errv4 = max(Err4);

%% Зависимость погрешности от заданной точности
clc, clear, close all

N = 1e3;
delta = 1e-6;
eps = 1e-12;
y0 = ones(10, 1);

A = rand(10);
[Q, R] = qr(A);
D1 = linspace(10, 1, 10);
D2 = linspace(10, 9, 10);
R1 = R-diag(diag(R))+diag(D1);
R2 = R-diag(diag(R))+diag(D2);
A1 = Q*R1*Q';
A2 = Q*R2*Q';

[V1, L1] = eig(A1);
m1 = max(L1(:));
[r1, c1] = find(L1 == m1);

[V2, L2] = eig(A2);
m2 = max(L2(:));
[r2, c2] = find(L2 == m2);

Errd1 = zeros(1, 10);
Errx1 = zeros(1, 10);

Errd2 = zeros(1, 10);
Errx2 = zeros(1, 10);

Eps = zeros(1, 10);

for k = 1:10
    
    [d1, x1, errd1] = PM_norm(A1, y0, N, delta, eps);
    [d2, x2, errd2] = PM_norm(A2, y0, N, delta, eps);
    errx1 = abs(sin(acos((V1(:, c1)'*x1)/(norm(V1(:, c1))*norm(x1)))));
    errx2 = abs(sin(acos((V2(:, c2)'*x2)/(norm(V2(:, c2))*norm(x2)))));
    
    Errd1(k) = errd1;
    Errd2(k) = errd2;
  
    Errx1(k) = errx1;
    Errx2(k) = errx2;
    
    Eps(k) = eps;
    eps = eps*10;
    
end

figure
loglog(Eps, Errd1);
xlabel('Eps');
ylabel('Errd');
title('График зависимости погрешности от заданной точности для матрицы с хорошей отделимостью');
legend('PMnorm');
grid minor

figure
loglog(Eps, Errx1);
xlabel('Eps');
ylabel('Errx');
title('График зависимости погрешности СВ от заданной точности для матрицы с хорошей отделимостью');
legend('PMnorm');
grid minor

figure
loglog(Eps, Errd2);
xlabel('Eps');
ylabel('Errd');
title('График зависимости погрешности от заданной точности для матрицы с плохой отделимостью');
legend('PMnorm');
grid minor

figure
loglog(Eps, Errx2);
xlabel('Eps');
ylabel('Errx');
title('График зависимости погрешности СВ от заданной точности для матрицы с плохой отделимостью');
legend('PMnorm');
grid minor

%% Зависимость числа итераций от заданной точности
clc, clear, close all

N = 1e3;
delta = 1e-6;
eps = 1e-12;
y0 = ones(10, 1);

A = rand(10);
[Q, R] = qr(A);
D1 = linspace(10, 2, 10);
D2 = linspace(10, 8, 10);
R1 = R-diag(diag(R))+diag(D1);
R2 = R-diag(diag(R))+diag(D2);
A1 = Q*R1*Q';
A2 = Q*R2*Q';

I1 = zeros(1, 10);
I2 = zeros(1, 10);
    
Eps = zeros(1, 10);

for k = 1:10
    
    [d1, x1, errd1, i1] = PM_norm(A1, y0, N, delta, eps);
    [d2, x2, errd2, i2] = PM_norm(A2, y0, N, delta, eps);
    
    I1(k) = i1;
    I2(k) = i2;
   
    Eps(k) = eps;
    eps = eps*10;
    
end

figure
semilogx(Eps, I1);
xlabel('Eps');
ylabel('I');
title('График зависимости числа итераций от заданной точности для матрицы с хорошей отделимостью');
legend('PMnorm');
grid minor

figure
semilogx(Eps, I2);
xlabel('Eps');
ylabel('I');
title('График зависимости числа итераций от заданной точности для матрицы с плохой отделимостью');
legend('PMnorm');
grid minor

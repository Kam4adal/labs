function [d, err] = QR(A, N)

A = hess(A);
n = size(A, 1);
d0 = ones(n, 1);

for i = 1:N
  
    [Q, R] = qr(A);
    A = R*Q;
    d = abs(diag(A));
    err = norm(d-d0);
    d0 = d;
end

end

clc, clear, close all

A1 = rand(100);
[Q1, R1] = qr(A1);
D1 = [linspace(10, 11, 99) 100];
R1 = R1-diag(diag(R1))+diag(D1);
A1 = Q1*R1*Q1';
A1 = hess(A1);
B1 = rand(100);

A2 = rand(500);
[Q2, R2] = qr(A2);
D2 = [linspace(50, 55, 499) 500];
R2 = R2-diag(diag(R2))+diag(D2);
A2 = Q2*R2*Q2';
A2 = hess(A2);
A3 = A2;
A3(250, 100) = 1e-16;
B2 = rand(500);

figure 
spy(A1)

figure
spy(A2)

figure
spy(A3)

% d = eig(A)
tic
d11 = eig(A1);
t11 = toc;

d11 = sort(abs(d11));
err11 = norm(D1'-d11);

tic
d21 = eig(A2);
t21 = toc;

d21 = sort(abs(d21));
err21 = norm(D2'-d21);

tic
d31 = eig(A3);
t31 = toc;

d31 = sort(abs(d31));
err31 = norm(D2'-d31);

% d = eig(A, B)
tic
d12 = eig(A1, B1);
t12 = toc;
 
tic
d22 = eig(A2, B2);
t22 = toc;

tic
d32 = eig(A3, B2);
t32 = toc;

% [V, D] = eig(A)
tic
[V13, D13] = eig(A1);
t13 = toc;

err13 = norm(A1*V13-V13*D13);

tic
[V23, D23] = eig(A2);
t23 = toc;

err23 = norm(A2*V23-V23*D23);

tic
[V33, D33] = eig(A3);
t33 = toc;

err33 = norm(A3*V33-V33*D33);

% [V, D] = eig(A, 'nobalance')
tic
[V14, D14] = eig(A1, 'nobalance');
t14 = toc;

err14 = norm(A1*V14-V14*D14);

tic
[V24, D24] = eig(A2, 'nobalance');
t24 = toc;

err24 = norm(A2*V24-V24*D24);

tic
[V34, D34] = eig(A3, 'nobalance');
t34 = toc;

err34 = norm(A3*V34-V34*D34);

% [V, D] = eig(A, B)
tic
[V15, D15] = eig(A1, B1);
t15 = toc;

err15 = norm(A1*V15-B1*V15*D15);

tic
[V25, D25] = eig(A2, B2);
t25 = toc;

err25 = norm(A2*V25-B2*V25*D25);

tic
[V35, D35] = eig(A3, B2);
t35 = toc;

err35 = norm(A3*V35-B2*V35*D35);

% [V, D] = eig(A, B, flag)
tic
[V16, D16] = eig(A1, B1, 'qz');
t16 = toc;

err16 = norm(A1*V16-B1*V16*D16);

tic
[V26, D26] = eig(A2, B2, 'qz');
t26 = toc;

err26 = norm(A2*V26-B2*V26*D26);

tic
[V36, D36] = eig(A3, B2, 'qz');
t36 = toc;

err36 = norm(A3*V36-B2*V36*D36);

err11
t11

err21
t21

err31
t31

t12

t22

t32

err13
t13

err23
t23

err33
t33

err14
t14

err24
t24

err34
t34

err15
t15

err25
t25

err35
t35

err16
t16

err26
t26

err36
t36
clc, clear, close all

A1 = rand(500);
[Q1, R1] = qr(A1);
D1 = linspace(50, 25000, 500);
R1 = R1-diag(diag(R1))+diag(D1);
A1 = Q1*R1*Q1';
A1 = sparse(A1);
A1 = hess(full(A1));
A2 = A1;
A2(250, 100) = 1e-16;
B = rand(500);
n = 500;

figure
spy(A1)

figure
spy(A2)

% [V, D, flag] = eigs(A, n)
tic
[V11, D11, flag11] = eigs(A1, n);
t11 = toc;

tic
[V21, D21, flag21] = eigs(A2, n);
t21 = toc;

% [V, D, flag] = eig(A, n, 'smallestabs')
tic
[V12, D12, flag12] = eigs(A1, n, 'smallestabs');
t12 = toc;

tic
[V22, D22, flag22] = eigs(A2, n, 'smallestabs');
t22 = toc;

% [V, D, flag] = eig(A, n, 'bothendsreal')
tic
[V13, D13, flag13] = eigs(A1, n, 'bothendsreal');
t13 = toc;

tic
[V23, D23, flag23] = eigs(A2, n, 'bothendsreal');
t23 = toc;

t11

t21

t12

t22

t13

t23

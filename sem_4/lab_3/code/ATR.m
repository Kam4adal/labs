function [I, i, H, X] = ATR(f, a, b, eps)
    
fa = f(a);
fb = f(b);

i = 0;
H = [];
X = [];

[m, fm, whole] = ATR_mem(f, a, fa, b, fb);
[I, i, H, X] = ATR_rec(f, a, fa, b, fb, eps, whole, m, fm, i, H, X);

end

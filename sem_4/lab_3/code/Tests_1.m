%% Зависимость фактической ошибки от заданной точности
clc, clear, close all

f = @(x) x.^4-3.5.*x.^3+2.5.*x.^2-7.*x-6.4;
a = 3;
b = 4;
I = 361/120;
N = 10^3;
eps = 1e-10;

Err = zeros(1, 8);
Eps = zeros(1, 8);

for k = 1:8
    
    I1 = TR(f, a, b, N, eps);
    
    err = abs(I-I1);
    Err(k) = err;
    
    Eps(k) = eps;
    eps = eps*10;
    
end

fb = @(x) x;

figure
plot(log2(Eps), log2(Err), 'o')
hold on
plot(log2(Eps), fb(log2(Eps)))
xlabel('eps');
ylabel('err');
title('Зависимость фактической ошибки от заданной точности');
legend('TR');
grid minor

%% Зависимость числа итераций от заданной точности
clc, clear, close all

f = @(x) x.^4-3.5.*x.^3+2.5.*x.^2-7.*x-6.4;
a = 3;
b = 4;
N = 10^3;
eps = 1e-10;

K = zeros(1, 8);
Eps = zeros(1, 8);

for i = 1:8
    
    [I1, k] = TR(f, a, b, N, eps);
    
    K(i) = k;
    
    Eps(i) = eps;
    eps = eps*10;
    
end

plot(log2(Eps), log2(K))
xlabel('eps');
ylabel('K');
title('Зависимость числа итераций от заданной точности');
legend('K(eps)');
grid minor

%% Зависимость фактической ошибки от длины отрезка разбиения
clc, clear, close all

f = @(x) x.^4-3.5.*x.^3+2.5.*x.^2-7.*x-6.4;
a = 3;
b = 4;
I = 361/120;
N = 10^3;
eps = 1e-10;
   
[I1, k, Iarr, H] = TR(f, a, b, N, eps);

n = length(Iarr);
Err = zeros(1, n);

for i = 1:n
    err = abs(I-Iarr(i));
    Err(i) = err;
end
    
figure
loglog(H, Err)
xlabel('h');
ylabel('err');
title('Зависимость фактической ошибки от длины отрезка разбиения');
legend('TR');
grid minor

%% Зависимость фактической ошибки от заданной точности(функция с разрывом производной)
clc, clear, close all

f = @(x) x.^4-3.5*abs(x-3.4).*x.^3+2.5.*x.^2-7.*x-6.4;
a = 3;
b = 4;
syms x
I = double(int(f, x, a, b));
N = 10^3;
eps = 1e-10;

Err = zeros(1, 8);
Eps = zeros(1, 8);

for k = 1:8
    
    I1 = TR(f, a, b, N, eps);
    
    err = abs(I-I1);
    Err(k) = err;
    
    Eps(k) = eps;
    eps = eps*10;
    
end

fb = @(x) x;

figure
plot(log2(Eps), log2(Err), 'o')
hold on
plot(log2(Eps), fb(log2(Eps)))
xlabel('eps');
ylabel('err');
title('Зависимость фактической ошибки от заданной точности');
legend('TR');
grid minor

%% Зависимость числа итераций от заданной точности(функция с разрывом)
clc, clear, close all

f = @(x) x.^4-3.5.*abs(x-3.4).*x.^3+2.5.*x.^2-7.*x-6.4;
a = 3;
b = 4;
N = 10^3;
eps = 1e-10;

K = zeros(1, 8);
Eps = zeros(1, 8);

for i = 1:8
    
    [I1, k] = TR(f, a, b, N, eps);
    
    K(i) = k;
    
    Eps(i) = eps;
    eps = eps*10;
    
end

plot(log2(Eps), log2(K))
xlabel('eps');
ylabel('K');
title('Зависимость числа итераций от заданной точности');
legend('TR');
grid minor

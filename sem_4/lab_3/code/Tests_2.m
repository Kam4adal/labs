%% Зависимость фактической ошибки от заданной точности
clc, clear, close all

f1 = @(x) sin(x.^4).*(x.^4-3.5.*x.^3+2.5.*x.^2-7.*x-6.4);
f2 = @(x) ((3-4.*x).^(-1/5));
f3 = @(x) 1/(x.^2+4.*x+3);

a1 = 1;
b1 = 2;

a2 = 3/4+1e-9;
b2 = 1;

a3 = 1;
b3 = 1e7;

syms x
It1 = int(f1, x, a1, b1);
It2 = 5/16;
It3 = 0.5*log(2);

eps = 1e-7;

Err1 = zeros(1, 5);
Err2 = zeros(1, 5);
Err3 = zeros(1, 5);

Eps = zeros(1, 5);

for k = 1:5
    
    I1 = ATR(f1, a1, b1, eps);
    I2 = ATR(f2, a2, b2, eps);
    I3 = ATR(f3, a3, b3, eps);
    
    err1 = abs(It1-I1);
    err2 = abs(It2-abs(I2));
    err3 = abs(It3-I3);
    
    Err1(k) = err1;
    Err2(k) = err2;
    Err3(k) = err3;
    
    Eps(k) = eps;
    eps = eps*10;

end

fx = @(x) x;

figure
plot(log2(Eps), log2(Err1), 'o', 'Color', 'red')
hold on
plot(log2(Eps), log2(Err2), 'o', 'Color', 'green')
plot(log2(Eps), log2(Err3), 'o', 'Color', 'blue')
plot(log2(Eps), fx(log2(Eps)))
xlabel('eps');
ylabel('err');
title('Зависимость фактической ошибки от заданной точности');
legend('f1', 'f2', 'f3');
grid minor

%% Зависимость числа итераций от заданной точности
clc, clear, close all

f1 = @(x) sin(x.^4).*(x.^4-3.5.*x.^3+2.5.*x.^2-7.*x-6.4);
f2 = @(x) ((3-4.*x).^(-1/5));
f3 = @(x) 1/(x.^2+4.*x+3);

a1 = 1;
b1 = 2;

a2 = 3/4+1e-9;
b2 = 1;

a3 = 1;
b3 = 1e7;
    
eps = 1e-7;

K1 = zeros(1, 5);
K2 = zeros(1, 5);
K3 = zeros(1, 5);

Eps = zeros(1, 5);

for i = 1:5
    
    [I1, k1] = ATR(f1, a1, b1, eps);
    [I2, k2] = ATR(f2, a2, b2, eps);
    [I3, k3] = ATR(f3, a3, b3, eps);
    
    K1(i) = k1;
    K2(i) = k2;
    K3(i) = k3;
    
    Eps(i) = eps;
    eps = eps*10;
    
end

figure
plot(log2(Eps), log2(K1), 'Color', 'red')
hold on
plot(log2(Eps), log2(K2), 'Color', 'green')
plot(log2(Eps), log2(K3), 'Color', 'blue')
xlabel('eps');
ylabel('K');
title('Зависимость числа итераций от заданной точности');
legend('f1', 'f2', 'f3');
grid minor

%% Зависимость фактической длины отрезка разбиения от координаты
clc, clear, close all

f1 = @(x) sin(x.^4).*(x.^4-3.5.*x.^3+2.5.*x.^2-7.*x-6.4);
f2 = @(x) ((3-4.*x).^(-1/5));
f3 = @(x) 1/(x.^2+4.*x+3);

a1 = 1;
b1 = 2;

a2 = 3/4+1e-9;
b2 = 1;

a3 = 1;
b3 = 1e7;
    
eps = 1e-7;
   
[I1, k1, H1, X1] = ATR(f1, a1, b1, eps);
[I2, k2, H2, X2] = ATR(f2, a2, b2, eps);
[I3, k3, H3, X3] = ATR(f3, a3, b3, eps);

figure
semilogy(X1, H1)
xlabel('x');
ylabel('H');
title('Зависимость фактической длины отрезка разбиения от координаты');
legend('f1');
grid minor

figure
semilogy(X2, H2)
xlabel('x');
ylabel('H');
title('Зависимость фактической длины отрезка разбиения от координаты');
legend('f2');
grid minor

figure
semilogy(X3, H3)
xlabel('x');
ylabel('H');
title('Зависимость фактической длины отрезка разбиения от координаты');
legend('f3');
grid minor

%% Зависимость фактической ошибки от заданной точности (для функциии с возмущением)
clc, clear, close all

a3 = 1;
b3 = 1e7;

It3 = 0.5*log(2);

eps = 1e-7;

for i = 1:3
    
    c = 4+4*(-i+(i+i)*rand())*0.01;
    f3 = @(x) 1/(x.^2+c.*x+3);
    
    Err = zeros(1, 5);
    Eps = zeros(1, 5);
            
        for k = 1:5
            
            I3 = ATR(f3, a3, b3, eps);
            
            err3 = abs(It3-I3);
           
            Err(k) = err3;
            
            Eps(k) = eps;
            eps = eps*10;
            
        end
        
        fx = @(x) x;
        
        figure
        plot(log2(Eps), log2(Err), 'o')
        hold on
        plot(log2(Eps), fx(log2(Eps)))
        xlabel('eps');
        ylabel('err');
        title('Зависимость фактической ошибки от заданной точности');
        legend('f3');
        grid minor
end

%% Зависимость фактической длины отрезка разбиения от координаты (функции с возмущением)
clc, clear, close all

a3 = 1;
b3 = 1e7;
    
eps = 1e-7;

H = [];
X = [];

for i = 1:3
    c = 4+4*(-i+(i+i)*rand())*0.01;
    f3 = @(x) 1/(x.^2+c.*x+3);
    
    [I3, k3, H3, X3] = ATR(f3, a3, b3, eps);
    
    figure
    semilogy(X3, H3)
    xlabel('x');
    ylabel('H');
    title('Зависимость фактической длины отрезка разбиения от координаты');
    legend('f3');
    grid minor

end

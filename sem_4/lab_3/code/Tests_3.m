%% Зависимость фактической ошибки от заданной точности
clc, clear, close all

f = @(x) cos(2.*x).*(x.^4-3.5.*x.^3+2.5.*x.^2-7.*x-6.4);
a = 5;
b = 10;
N = 1e3;
eps = 1e-10;

syms x
It = int(f, x, a, b);

Err = zeros(1, 5);
Eps = zeros(1, 5);

for i = 1:5
    
    I = Lobatto(f, a, b, N, eps);
  
    Err(i) = abs(It - I);
    
    Eps(i) = eps;
    eps = eps*100;
    
end

fb = @(x) x;

figure
plot(log2(Eps), log2(Err), 'o')
hold on
plot(log2(Eps), fb(log2(Eps)))
xlabel('eps');
ylabel('err');
title('Зависимость фактической ошибки от заданной точности');
legend('Lobatto');
grid minor

%% Зависимость фактической ошибки от заданной точности (для уточнённого значения интеграла)
clc, clear, close all

f = @(x) cos(2.*x).*(x.^4-3.5.*x.^3+2.5.*x.^2-7.*x-6.4);
a = 5;
b = 10;
N = 1e3;
eps = 1e-10;

syms x
It = int(f, x, a, b);

Err = zeros(1, 5);
Eps = zeros(1, 5);

for i = 1:5
    
    [I, R, H, k, E, Iarr] = Lobatto(f, a, b, N, eps);
    
    err = abs(It-R);
    Err(i) = err;
    
    Eps(i) = eps;
    eps = eps*100;
    
end

fb = @(x) x;

figure
plot(log2(Eps), log2(Err), 'o')
hold on
plot(log2(Eps), fb(log2(Eps)))
xlabel('eps');
ylabel('err');
title('Зависимость фактической ошибки от заданной точности для уточненного значения интеграла');
legend('Lobatto');
grid minor

%% Зависимость числа итераций от заданной точности
clc, clear, close all

f = @(x) cos(2.*x).*(x.^4-3.5.*x.^3+2.5.*x.^2-7.*x-6.4);
a = 5;
b = 10;
N = 1e3;
eps = 1e-10;

K = zeros(1, 5);
Eps = zeros(1, 5);

for i = 1:5
    
    [I, R, H, k, E, Iarr] = Lobatto(f, a, b, N, eps);
    
    K(i) = k;
    
    Eps(i) = eps;
    eps = eps*100;
    
end

plot(log2(Eps), log2(K))
xlabel('eps');
ylabel('K');
title('Зависимость числа итераций от заданной точности');
legend('Lobatto');
grid minor

%% Зависимость фактической ошибки от длины отрезка разбиения
clc, clear, close all

f = @(x) cos(2.*x).*(x.^4-3.5.*x.^3+2.5.*x.^2-7.*x-6.4);
a = 5;
b = 10;
N = 1e3;
eps = 1e-10;

syms x
It = int(f, x, a, b);
   
[I, R, H, k, E, Iarr] = Lobatto(f, a, b, N, eps);

n = length(Iarr);
Err = zeros(1, n);

for i = 1:n
    err = abs(It-Iarr(i));
    Err(i) = err;
end
    
figure
loglog(H, Err)
xlabel('h');
ylabel('err');
title('Зависимость фактической ошибки от длины отрезка разбиения');
legend('Lobatto');
grid minor

%% Зависимость фактической ошибки от заданной точности(функция с разрывом производной)
clc, clear, close all

f = @(x) cos(2.*x).*(x.^4-3.5.*abs(x-7.49).*x.^3+2.5.*x.^2-7.*x-6.4);
a = 5;
b = 10;
N = 1e3;
eps = 1e-10;

syms x
It = double(int(f, x, a, b));

Err = zeros(1, 5);
Eps = zeros(1, 5);

for i = 1:5
    
    [I, R, H, k, E, Iarr] = Lobatto(f, a, b, N, eps);
    
    err = abs(It-I);
    Err(i) = err;
    
    Eps(i) = eps;
    eps = eps*100;
    
end

fb = @(x) x;

figure
plot(log2(Eps), log2(Err), 'o')
hold on
plot(log2(Eps), fb(log2(Eps)))
xlabel('eps');
ylabel('err');
title('Зависимость фактической ошибки от заданной точности');
legend('Lobatto');
grid minor

%% Зависимость числа итераций от заданной точности(функция с разрывом производной)
clc, clear, close all

f = @(x) cos(2.*x).*(x.^4-3.5.*abs(x-7.49).*x.^3+2.5.*x.^2-7.*x-6.4);
a = 5;
b = 10;
N = 1e3;
eps = 1e-10;

K = zeros(1, 5);
Eps = zeros(1, 5);

for i = 1:5
    
    [I, R, H, k, E, Iarr] = Lobatto(f, a, b, N, eps);
    
    K(i) = k;
    
    Eps(i) = eps;
    eps = eps*100;
    
end

plot(log2(Eps), log2(K))
xlabel('eps');
ylabel('K');
title('Зависимость числа итераций от заданной точности');
legend('Lobatto');
grid minor

%% Зависимость фактической ошибки от заданной точности для уточнённого значения интеграла(функция с разрывом производной)
clc, clear, close all

f = @(x) cos(2.*x).*(x.^4-3.5.*abs(x-7.49).*x.^3+2.5.*x.^2-7.*x-6.4);
a = 5;
b = 10;
N = 1e3;
eps = 1e-10;

syms x
It = double(int(f, x, a, b));

Err = zeros(1, 5);
Eps = zeros(1, 5);

for i = 1:5
    
    [I, R, H, k, E, Iarr] = Lobatto(f, a, b, N, eps);
    
    err = abs(It-R);
    Err(i) = err;
    
    Eps(i) = eps;
    eps = eps*100;
    
end

fb = @(x) x;

figure
plot(log2(Eps), log2(Err), 'o')
hold on
plot(log2(Eps), fb(log2(Eps)))
xlabel('eps');
ylabel('err');
title('Зависимость фактической ошибки от заданной точности');
legend('Lobatto');
grid minor

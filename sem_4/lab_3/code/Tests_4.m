%% P(x)
clc, clear, close all

f1 = @(x) x.^4-3.5.*x.^3+2.5.*x.^2-7.*x-6.4;
f2 = @(x) x.^4-3.5.*abs(x-2.49).*x.^3+2.5.*x.^2-7.*x-6.4;

a = 0;
b = 5;

syms x
It1 = int(f1, x, a, b);
It2 = int(f2, x, a, b);

p = 10;

P = zeros(1, 8);
Err1 = zeros(1, 8);
Err2 = zeros(1, 8);

for i = 1:8
    
    I1 = integral(f1, a, b, 'AbsTol', 10^(-p));
    I2 = integral(f2, a, b, 'AbsTol', 10^(-p));
    
    err1 = abs(It1 - I1);
    err2 = abs(It2 - I2);
    
    Err1(i) = err1;
    Err2(i) = err2;
    
    P(i) = p;
    p = p - 1;
end

figure
plot(P, Err1)
xlabel('p');
ylabel('err');
title('Зависимость погрешности от p');
legend('f1');
grid minor

figure
plot(P, Err2)
xlabel('p');
ylabel('err');
title('Зависимость погрешности от p');
legend('f2');
grid minor

%% P(x)*sin(x^2)
clc, clear, close all

f = @(x) sin(x.^2).*(x.^4-3.5.*x.^3+2.5.*x.^2-7.*x-6.4);

a = 0;
b = 10;

syms x
It = int(f, x, a, b);

p = 10;

P = zeros(1, 8);
Err1 = zeros(1, 8);
Err2 = zeros(1, 8);

for i = 1:8
    
    I1 = integral(f, a, b, 'AbsTol', 10^(-p));
    I2 = quadgk(f, a, b);
    
    err1 = abs(It - I1);
    err2 = abs(It - I2);
    
    Err1(i) = err1;
    Err2(i) = err2;
    
    P(i) = p;
    p = p - 1;
end

figure
plot(P, Err1)
xlabel('p');
ylabel('err');
title('Зависимость погрешности от p');
legend('f');
grid minor

figure
plot(P, Err2)
xlabel('p');
ylabel('err');
title('Зависимость погрешности от p');
legend('f');
grid minor

%% P2(x), P4(x)
clc, clear, close all

f1 = @(x, y) x.^(y.^3)-3.5.*x.^3+2.5.*x.^2-7.*x-6.4;
f2 = @(x, y, z) x.^(y.^3)-3.5.*x.^3+2.5.*x.^2-7.*x-6.4*x.^z;

a = -1;
b = 1;

c = 0;
d = 1;

e = 0;
f = 1;

syms x
It1 = integral2(f1, a, b, c, d);
It2 = integral3(f2, a, b, c, d, e, f);

p = 10;

P = zeros(1, 8);
Err1 = zeros(1, 8);
Err2 = zeros(1, 8);

for i = 1:8
    
    I1 = integral2(f1, a, b, c, d, 'AbsTol', 10^(-p));
    I2 = integral3(f2, a, b, c, d, e, f, 'AbsTol', 10^(-p));
    
    err1 = abs(It1 - I1);
    err2 = abs(It2 - I2);
    
    Err1(i) = err1;
    Err2(i) = err2;
    
    P(i) = p;
    p = p - 1;
end

figure
plot(P, Err1)
xlabel('p');
ylabel('err');
title('Зависимость погрешности от p');
legend('f1');
grid minor

figure
plot(P, Err2)
xlabel('p');
ylabel('err');
title('Зависимость погрешности от p');
legend('f2');
grid minor

%% P3(x)
clc, clear, close all

f = @(x,y) (x>=-2 & x<=10 & y>=0 & y<=1).*x.^(y.^3)-3.5.*x.^3+2.5.*x.^2-7.*x-6.4;

a = -1;
b = 1;

c = 0;
d = 1;

It = integral2(f, a, b, c, d, 'AbsTol', eps);

p = 10;

P = zeros(1, 8);
Err = zeros(1, 8);

for i = 1:8
    
    I = integral2(f, a, b, c, d, 'AbsTol', 10^(-p));
    
    err = abs(It - I);
    
    Err(i) = err;
    
    P(i) = p;
    p = p - 1;
end

figure
plot(P, Err)
xlabel('p');
ylabel('err');
title('Зависимость погрешности от p');
legend('f1');
grid minor

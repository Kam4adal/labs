function [t, y] = Euler_exp_sys(A, a, b, y0, h)

t = a:h:b;
y = zeros(length(y0), length(t));
y(:, 1) = y0;

for i = 1:length(t)-1
    y(:,i+1) = y(:,i) + h*A*y(:,i);
end

end
function [t, y] = Euler_implicit(f, a, b, y0, h)

t = a:h:b;
y = zeros(length(y0), length(t));
y(:, 1) = y0;

for i = 1:length(t)-1
    F = @(x) x - y(:, i) - h*f(t(i+1), x);
    y(:, i+1) = bisection(F, a, b, 1e-15, 1e3);
end

end
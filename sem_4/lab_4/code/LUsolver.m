function x = LUsolver(A, b)

n = size(A, 1);

L = eye(n);
U = A;

for k = 1:n-1
    for j = k+1:n
        L(j, k) = U(j, k)/U(k, k);
        U(j, k:n) = U(j, k:n) - L(j, k)*U(k, k:n);
    end
end

y = zeros(n, 1);
y(1) = b(1);

for i = 2:n
    s1 = L(i, 1:i-1)*y(1:i-1);
    y(i) = b(i) - s1;
end

x = zeros(n, 1);
x(n) =  y(n)/U(n, n);

for j = n:-1:2
    s2 = U(j-1, n:-1:j)*x(n:-1:j);
    x(j-1) = (y(j-1) - s2)/U(j-1, j-1);
end

end
function dydt = ODEfun(t, y)

dydt = zeros(5, 1);

dydt(1) = -2*y(1) + 3*y(2) - 4*y(3) + 5*y(4) - 6*y(5);
dydt(2) = -y(1) + 2*y(2) - 3*y(3) + 4*y(4) - 5*y(5);
dydt(3) = y(1) - y(2) + y(3) - y(4) + y(5);
dydt(4) = -y(1) + y(2) - y(3) + y(4) - y(5);
dydt(5) = y(1) - y(2) + y(3) - y(4) + 2*y(5);
    
end
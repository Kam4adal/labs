function [yh, Xh, Yh, H, XX, YY] = RK3(f, a, b, y0, N, eps)
 
h = b - a;
n = 1;

x0 = a;

y2h = RK3_calc(f, x0, y0, h, n);

H = [];
XX = {};
YY = {};

for k = 1:N
    
    h = h/2;
    n = n*2;
    
    [yh, Xh, Yh] = RK3_calc(f, x0, y0, h, n);
    
    H = [H h];
    XX{k} = Xh;
    YY{k} = Yh;
    
    err = abs(y2h - yh)/7;
    
    if err < eps
        return
    end
    
    y2h = yh;
   
end

end
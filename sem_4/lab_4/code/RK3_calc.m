function [y0, X, Y] = RK3_calc(f, x0, y0, h, n)

X = zeros(1, n+1);
Y = zeros(length(y0), n+1);

X(1) = x0;
Y(:, 1) = y0;

for i = 1:n
    
    k1 = h*f(x0, y0);
    k2 = h*f(x0+h/2, y0+k1/2);
    k3 = h*f(x0+h, y0-k1+2*k2);
    dy = (k1 + 4*k2 + k3)/6;
    
    x0 = x0 + h;
    X(i+1) = x0;
    
    y0 = y0 + dy;
    Y(:, i+1) = y0;
    
end

end
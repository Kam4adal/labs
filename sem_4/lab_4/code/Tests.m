%% Задача Коши
clc, clear, close all

f = @(x, y) -y.^2.*(log(x) + 2).*log(x)./x + y./x;
y0 = 1;
a = 1;
b = 2;
N = 1e3;
eps = 1e-7;

[yh, Xh, Yh, H, XX, YY] = RK3(f, a, b, y0, N, eps);

figure
plot(Xh, Yh, 'b')
xlabel('x');
ylabel('y');
title('График решения задачи Коши');
legend('f');
grid minor

%% Графики численного и точного решений для 2-х значений шага на отрезке
clc, clear, close all

f = @(x, y) -y.^2.*(log(x) + 2).*log(x)./x + y./x;
ft = @(x) x./(x.*log(x).^2 + 1);
y0 = 1;
a = 1;
b = 2;
N = 1e3;
eps = 1e-3;

[yh, Xh, Yh, H, XX, YY] = RK3(f, a, b, y0, N, eps);

X = a:0.01:b;

figure
plot(Xh, Yh, 'r')
hold on
plot(X, ft(X), 'b')
xlabel('x');
ylabel('y');
title('График численного и точного решений');
legend('Численное решение', 'Точное решение');
grid minor

figure
plot(XX{2}, YY{2}, 'r')
hold on
plot(X, ft(X), 'b')
xlabel('x');
ylabel('y');
title('График численного и точного решений');
legend('Численное решение', 'Точное решение');
grid minor

%% График ошибки на заданном отрезке
clc, clear, close all

f = @(x, y) -y.^2.*(log(x) + 2).*log(x)./x + y./x;
ft = @(x) x./(x.*log(x).^2 + 1);
y0 = 1;
a = 1;
b = 2;
N = 1e3;
eps = 1e-7;

[yh, Xh, Yh, H, XX, YY] = RK3(f, a, b, y0, N, eps);

Err = abs(Yh - ft(Xh));

figure
plot(Xh, Err, 'b')
xlabel('x');
ylabel('err');
title('График ошибки на отрезке');
legend('err(x)');
grid minor

%% График фактической точности от заданной точности
clc, clear, close all

f = @(x, y) -y.^2.*(log(x) + 2).*log(x)./x + y./x;
ft = @(x) x./(x.*log(x).^2 + 1);
y0 = 1;
a = 1;
b = 2;
N = 1e3;
eps = 1e-7;

Err = zeros(1, 5);
Eps = zeros(1, 5);

for i = 1:5
    
    [yh, Xh, Yh, H, XX, YY] = RK3(f, a, b, y0, N, eps);
    Yt = ft(Xh);
    
    err = max(abs(Yh - Yt));
    Err(i) = err;
    
    Eps(i) = eps;
    eps = eps*10;
    
end

fx = @(x) x;

figure
loglog(Eps, Err, 'o')
hold on 
loglog(Eps, fx(Eps))
xlabel('eps');
ylabel('err');
title('График фактической точности от заданной точности');
legend('err(eps)');
grid minor

%% График фактической точности от величины шага
clc, clear, close all

f = @(x, y) -y.^2.*(log(x) + 2).*log(x)./x + y./x;
ft = @(x) x./(x.*log(x).^2 + 1);
y0 = 1;
a = 1;
b = 2;
N = 1e3;
eps = 1e-7;

[yh, Xh, Yh, H, XX, YY] = RK3(f, a, b, y0, N, eps);

n = length(XX);
Err = zeros(1, n);

for i = 1:n
    Yt = ft(XX{i});
    err = max(abs(Yt - YY{i}));
    Err(i) = err;
end

figure
loglog(H, Err)
xlabel('h');
ylabel('err');
title('График фактической точности от величины шага');
legend('err(h)');
grid minor

%% График численного решения системы дифференциальных уравнений

y0 = rand(5, 1);
a = 1;
b = 2;
N = 1e3;
eps = 1e-7;

[t, y] = ode23(@ODEfun, [a b], y0);
[yh, Xh, Yh, H, XX, YY] = RK3(@ODEfun, a, b, y0, N, eps);

figure
plot(Xh, Yh(1, :), Xh, Yh(2, :), Xh, Yh(3, :), Xh, Yh(4, :), Xh, Yh(5, :))
xlabel('x');
ylabel('y');
title('График численного решения системы ДУ');
legend('RK3');
grid minor

X2h = XX{2};
Y2h = YY{2};

figure
plot(X2h, Y2h(1, :), X2h, Y2h(2, :), X2h, Y2h(3, :), X2h, Y2h(4, :), X2h, Y2h(5, :))
xlabel('x');
ylabel('y');
title('График численного решения системы ДУ');
legend('RK3');
grid minor

figure 
plot(t, y(:, 1), t, y(:, 2), t, y(:, 3), t, y(:, 4), t, y(:, 5))
xlabel('x');
ylabel('y');
title('График численного решения системы ДУ');
legend('ode23');
grid minor

%% График фактической точности от заданной точности для системы ДУ
clc, clear, close all

y0 = rand(5, 1);
a = 1;
b = 2;
N = 1e3;
eps = 1e-7;

syms y1(t) y2(t) y3(t) y4(t) y5(t)
eqn1 = diff(y1,t) == -2*y1 + 3*y2 - 4*y3 + 5*y4 - 6*y5;
eqn2 = diff(y2,t) == -y1 + 2*y2 - 3*y3 + 4*y4 - 5*y5;
eqn3 = diff(y3,t) == y1 - y2 + y3 - y4 + y5;
eqn4 = diff(y4,t) == -y1 + y2 - y3 + y4 - y5;
eqn5 = diff(y5,t) == y1 - y2 + y3 - y4 + 2*y5;
eqns = [eqn1, eqn2, eqn3, eqn4, eqn5];
cond1 = y1(1) == y0(1);
cond2 = y2(1) == y0(2);
cond3 = y3(1) == y0(3);
cond4 = y4(1) == y0(4);
cond5 = y5(1) == y0(5);
conds = [cond1, cond2, cond3, cond4, cond5];
sol = dsolve(eqns, conds);
y1 = matlabFunction(sol.y1);
y2 = matlabFunction(sol.y2);
y3 = matlabFunction(sol.y3);
y4 = matlabFunction(sol.y4);
y5 = matlabFunction(sol.y5);

Err1 = zeros(1, 5);
Err2 = zeros(1, 5);
Err3 = zeros(1, 5);
Err4 = zeros(1, 5);
Err5 = zeros(1, 5);

Eps = zeros(1, 5);

for i = 1:5
    
    [yh, Xh, Yh, H, XX, YY] = RK3(@ODEfun, a, b, y0, N, eps);
   
    err1 = max(abs(Yh(1, :) - y1(Xh)));
    err2 = max(abs(Yh(2, :) - y2(Xh)));
    err3 = max(abs(Yh(3, :) - y3(Xh)));
    err4 = max(abs(Yh(4, :) - y4(Xh)));
    err5 = max(abs(Yh(5, :) - y5(Xh)));
    
    Err1(i) = err1;
    Err2(i) = err2;
    Err3(i) = err3;
    Err4(i) = err4;
    Err5(i) = err5;
    
    Eps(i) = eps;
    eps = eps*10;
    
end

fx = @(x) x;

figure
loglog(Eps, Err1, 'o')
hold on 
loglog(Eps, Err2, 'o')
loglog(Eps, Err3, 'o')
loglog(Eps, Err4, 'o')
loglog(Eps, Err5, 'o')
loglog(Eps, fx(Eps))
xlabel('eps');
ylabel('err');
title('График фактической точности от заданной точности для системы');
legend('err1(eps)', 'err2(eps)', 'err3(eps)', 'err4(eps)', 'err5(eps)', 'fx', 'Location', 'NorthWest');
grid minor

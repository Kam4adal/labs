%% График фактической точности от заданной точности
clc, clear, close all

f = @(x, y) -y.^2.*(log(x) + 2).*log(x)./x + y./x;
ft = @(x) x./(x.*log(x).^2 + 1);
y0 = 1;
a = 1;
b = 2;
eps = 1e-7;

Err1 = zeros(1, 5);
Err2 = zeros(1, 5);
Err3 = zeros(1, 5);
Err4 = zeros(1, 5);
Err5 = zeros(1, 5);
Err6 = zeros(1, 5);
Err7 = zeros(1, 5);

I1 = zeros(1, 5);
I2 = zeros(1, 5);
I3 = zeros(1, 5);
I4 = zeros(1, 5);
I5 = zeros(1, 5);
I6 = zeros(1, 5);
I7 = zeros(1, 5);
    
Eps = zeros(1, 5);

for i = 1:5
    
    opts = odeset('RelTol', eps, 'AbsTol', eps);
    
    [t1, y1] = ode23(f, [a b], y0, opts);
    [t2, y2] = ode45(f, [a b], y0, opts);
    [t3, y3] = ode113(f, [a b], y0, opts);
    [t4, y4] = ode15s(f, [a b], y0, opts);
    [t5, y5] = ode23s(f, [a b], y0, opts);
    [t6, y6] = ode23t(f, [a b], y0, opts);
    [t7, y7] = ode23tb(f, [a b], y0, opts);
    
    Err1(i) = max(abs(ft(t1)-y1));
    Err2(i) = max(abs(ft(t2)-y2));
    Err3(i) = max(abs(ft(t3)-y3));
    Err4(i) = max(abs(ft(t4)-y4));
    Err5(i) = max(abs(ft(t5)-y5));
    Err6(i) = max(abs(ft(t6)-y6));
    Err7(i) = max(abs(ft(t7)-y7));
    
    I1(i) = length(t1);
    I2(i) = length(t2);
    I3(i) = length(t3);
    I4(i) = length(t4);
    I5(i) = length(t5);
    I6(i) = length(t6);
    I7(i) = length(t7);
    
    Eps(i) = eps;
    eps = eps*10;
    
end

figure
loglog(Eps, Err1, 'o', Eps, Err2, 'o', Eps, Err3, 'o', Eps, Err4, 'o', Eps, Err5, 'o', Eps, Err6, 'o', Eps, Err7, 'o')
xlabel('eps');
ylabel('err');
title('График фактической точности от заданной точности');
legend('ode23', 'ode45', 'ode113', 'ode15s', 'ode23s', 'ode23t', 'ode23tb', 'Location', 'NorthWest');
grid minor

figure
loglog(Eps, I1, Eps, I2, Eps, I3, Eps, I4, Eps, I5, Eps, I6, Eps, I7)
xlabel('eps');
ylabel('err');
title('График числа итераций от заданной точности');
legend('ode23', 'ode45', 'ode113', 'ode15s', 'ode23s', 'ode23t', 'ode23tb');
grid minor

%% График изменения шага по отрезку
clc, clear, close all

f = @(x, y) -y.^2.*(log(x) + 2).*log(x)./x + y./x;
ft = @(x) x./(x.*log(x).^2 + 1);
y0 = 1;
a = 1;
b = 2;
N = 1e3;
eps = 1e-7;

opts = odeset('RelTol', eps, 'AbsTol', eps);

[t1, y1] = ode23(f, [a b], y0, opts);
[t2, y2] = ode45(f, [a b], y0, opts);
[t3, y3] = ode113(f, [a b], y0, opts);
[t4, y4] = ode15s(f, [a b], y0, opts);
[t5, y5] = ode23s(f, [a b], y0, opts);
[t6, y6] = ode23t(f, [a b], y0, opts);
[t7, y7] = ode23tb(f, [a b], y0, opts);

H1 = zeros(length(t1), 1);

for i = 1:length(t1)-1
    H1(i) = t1(i+1) - t1(i);
end

H2 = zeros(length(t2), 1);

for i = 1:length(t2)-1
    H2(i) = t2(i+1) - t2(i);
end

H3 = zeros(length(t3), 1);

for i = 1:length(t3)-1
    H3(i) = t3(i+1) - t3(i);
end

H4 = zeros(length(t4), 1);

for i = 1:length(t4)-1
    H4(i) = t4(i+1) - t4(i);
end

H5 = zeros(length(t5), 1);

for i = 1:length(t5)-1
    H5(i) = t5(i+1) - t5(i);
end

H6 = zeros(length(t6), 1);

for i = 1:length(t6)-1
    H6(i) = t6(i+1) - t6(i);
end

H7 = zeros(length(t7), 1);

for i = 1:length(t7)-1
    H7(i) = t7(i+1) - t7(i);
end

figure
loglog(t1, H1, t2, H2, t3, H3, t4, H4, t5, H5, t6, H6, t7, H7)
xlabel('x');
ylabel('h');
title('График изменения шага по отрезку');
legend('ode23', 'ode45', 'od113', 'ode15s', 'ode23s', 'ode23t', 'ode23tb');
grid minor

%% Явный метод Эйлера
clc, clear, close all

f = @(x, y) 5*(y - x.^2);
ft = @(x) x.^2 + 0.4.*x + 0.08;
y0 = 0.08;
a = 0;
b = 1;

h = 0.08;
X = {};
Y = {};

for i = 1:8
    [x, y] = Euler_explicit(f, a, b, y0, h);
    X{i} = x;
    Y{i} = y;
    h = h - 0.01;
    b = b + 0.01;
end

figure 
plot(X{1}, Y{1}, X{2}, Y{2}, X{3}, Y{3}, X{4}, Y{4}, X{5}, Y{5}, X{6}, Y{6}, X{7}, Y{7}, X{8}, Y{8})
xlabel('x');
ylabel('y');
title('График решения для разного шага и отрезка интегрирования');
legend('h = 0.08, [a, b] = [0, 1]', 'h = 0.07, [a, b] = [0, 1.1]', 'h = 0.06, [a, b] = [0, 1.2]', 'h = 0.05, [a, b] = [0, 1.3]', 'h = 0.04, [a, b] = [0, 1.4]', 'h = 0.03, [a, b] = [0, 1.5]', 'h = 0.02, [a, b] = [0, 1.6]', 'Location', 'NorthWest')
grid minor

a1 = 0.08;
b1 = 0.5;

h1 = 0.08;

X1 = {};
Y1 = {};

for i = 1:8
    [x1, y1] = Euler_explicit(f, a1, b1, y0, h1);
    X1{i} = x1;
    Y1{i} = y1;
    h1 = h1 - 0.01;
end

figure 
plot(X1{1}, Y1{1}, X1{2}, Y1{2}, X1{3}, Y1{3}, X1{4}, Y1{4}, X1{5}, Y1{5}, X1{6}, Y1{6}, X1{7}, Y1{7}, X1{8}, Y1{8})
xlabel('x');
ylabel('y');
title('График решения на отрезке [0.08, 0.5] для разного шага');
legend('h = 0.08', 'h = 0.07', 'h = 0.06', 'h = 0.05', 'h = 0.04', 'h = 0.03', 'h = 0.02', 'h = 0.01', 'Location', 'NorthWest')
grid minor

a2 = 0.08;
b2 = 0.5;

Hh = {};
Errh = {};

for k = 1:3
    
    Err = zeros(1, 7);
    H = zeros(1, 7);
    h2 = 0.08;
    
    for i = 1:7
        [x2, y2] = Euler_explicit(f, a2, b2, y0, h2);
        err = max(abs(ft(x2) - y2));
        Err(i) = err;
        H(i) = h2;
        h2 = h2 - 0.01;
    end
    
    Errh{k} = Err;
    Hh{k} = H;
    
    b2 = b2 + 0.1;
end

figure 
plot(Hh{1}, Errh{1}, Hh{2}, Errh{2}, Hh{3}, Errh{3})
xlabel('x');
ylabel('err');
title('График погрешности в зависимости от шага');
legend('[a, b] = [0.08, 0.5]', '[a, b] = [0.08, 0.6]', '[a, b] = [0.08, 0.7]')
grid minor

%% Явный и неявный методы Эйлера
clc, clear, close all

f = @(x, y) -100*y + 10;
ft = @(x) 1/10 + 9*exp(-100*x)/10;
y0 = 1;
a = 0;
b = 10;

h = 0.08;
X1 = {};
Y1 = {};
X2 = {};
Y2 = {};

for i = 1:8
    [x1, y1] = Euler_explicit(f, a, b, y0, h);
    [x2, y2] = Euler_implicit(f, a, b, y0, h);
    X1{i} = x1;
    Y1{i} = y1;
    X2{i} = x2;
    Y2{i} = y2;
    h = h - 0.01;
end

figure 
plot(X1{1}, Y1{1}, X1{2}, Y1{2}, X1{3}, Y1{3}, X1{4}, Y1{4}, X1{5}, Y1{5}, X1{6}, Y1{6}, X1{7}, Y1{7}, X1{8}, Y1{8})
xlabel('x');
xlim([0 15]);
ylabel('y');
title('График решения на отрезке [0, 10] для разного шага (явный метод)');
legend('h = 0.08', 'h = 0.07', 'h = 0.06', 'h = 0.05', 'h = 0.04', 'h = 0.03', 'h = 0.02', 'h = 0.01', 'Location', 'NorthWest')
grid minor

figure
plot(X2{1}, Y2{1}, X2{2}, Y2{2}, X2{3}, Y2{3}, X2{4}, Y2{4}, X2{5}, Y2{5}, X2{6}, Y2{6}, X2{7}, Y2{7}, X2{8}, Y2{8})
xlabel('x');
ylabel('y');
title('График решения на отрезке [0, 10] для разного шага (неявный метод)');
legend('h = 0.08', 'h = 0.07', 'h = 0.06', 'h = 0.05', 'h = 0.04', 'h = 0.03', 'h = 0.02', 'h = 0.01')
grid minor

a1 = 0.08;
b1 = 1;

h1 = 0.08;

X1 = {};
Y1 = {};
X2 = {};
Y2 = {};

for i = 1:8
    [x1, y1] = Euler_explicit(f, a1, b1, y0, h1);
    [x2, y2] = Euler_implicit(f, a1, b1, y0, h1);
    X1{i} = x1;
    Y1{i} = y1;
    X2{i} = x2;
    Y2{i} = y2;
    h1 = h1 - 0.01;
end

figure 
plot(X1{1}, Y1{1}, X1{2}, Y1{2}, X1{3}, Y1{3}, X1{4}, Y1{4}, X1{5}, Y1{5}, X1{6}, Y1{6}, X1{7}, Y1{7}, X1{8}, Y1{8})
xlabel('x');
ylabel('y');
title('График решения на отрезке [0.08, 1] для разного шага (явный метод)');
legend('h = 0.08', 'h = 0.07', 'h = 0.06', 'h = 0.05', 'h = 0.04', 'h = 0.03', 'h = 0.02', 'h = 0.01', 'Location', 'NorthWest')
grid minor

figure
plot(X2{1}, Y2{1}, X2{2}, Y2{2}, X2{3}, Y2{3}, X2{4}, Y2{4}, X2{5}, Y2{5}, X2{6}, Y2{6}, X2{7}, Y2{7}, X2{8}, Y2{8})
xlabel('x');
ylabel('y');
title('График решения на отрезке [0.08, 1] для разного шага (неявный метод)');
legend('h = 0.08', 'h = 0.07', 'h = 0.06', 'h = 0.05', 'h = 0.04', 'h = 0.03', 'h = 0.02', 'h = 0.01')
grid minor

a2 = 0;
b2 = 1;

h2 = 0.08;

Err1 = zeros(1, 8);
Err2 = zeros(1, 8);
H = zeros(1, 8);

for i = 1:8
    [x1, y1] = Euler_explicit(f, a2, b2, y0, h2);
    [x2, y2] = Euler_implicit(f, a2, b2, y0, h2);
    
    err1 = max(abs(ft(x1) - y1));
    err2 = max(abs(ft(x2) - y2));
    Err1(i) = err1;
    Err2(i) = err2;
    
    H(i) = h2;
    h2 = h2 - 0.01;
end

figure
semilogy(H, Err1, 'o', H, Err2, 'o')
xlabel('x');
ylabel('err');
title('График погрешности в зависимости от шага');
legend('Явный метод', 'Неявный метод', 'Location', 'NorthWest')
grid minor

%% Системы ОДУ
clc, clear, close all

y0 = [1; 0; 1; 0; 1; 0; 1; 0; 1; 0];
S = linspace(1e2, 1e6, 20);
a = 0;
b = 1;

E1 = {};
E2 = {};

for i = 1:10
    
    h = 0.1;
    
    Err1 = zeros(1, 20);
    Err2 = zeros(1, 20);
    
    for j = 1:20
        
        D = linspace(-1, -S(i), 10);
        D = diag(D);
        V = rand(10);
        A = V*D*V^(-1);
        
        f = @(x, y) A*y;
        tspan = a:h:b;
        [X, Y] = ode15s(f, tspan, y0);
      
        [t1, y1] = Euler_exp_sys(A, a, b, y0, h);
        [t2, y2] = Euler_imp_sys(A, a, b, y0, h);

        Err1(j) = norm(Y - y1');
        Err2(j) = norm(Y - y2');
        
    end
   
    E1{i} = Err1;
    E2{i} = Err2;
    
    h = h - 0.01;
    
end   

figure 
loglog(S, E2{1}, S, E2{2}, S, E2{3}, S, E2{4}, S, E2{5}, S, E2{6}, S, E2{7}, S, E2{8}, S, E2{9}, S, E2{10})
xlabel('s');
ylabel('err');
title('График зависимости погрешности от жёсткости для разного шага (неявный метод)');
legend('h = 0.1', 'h = 0.09', 'h = 0.08', 'h = 0.07', 'h = 0.06', 'h = 0.05', 'h = 0.04', 'h = 0.03', 'h = 0.02', 'h = 0.01', 'Location', 'NorthWest')
grid minor

figure 
loglog(S, E1{1}, S, E1{2}, S, E1{3}, S, E1{4}, S, E1{5}, S, E1{6}, S, E1{7}, S, E1{8}, S, E1{9}, S, E1{10})
xlabel('s');
ylabel('err');
title('График зависимости погрешности от жёсткости для разного шага (явный метод)');
legend('h = 0.1', 'h = 0.09', 'h = 0.08', 'h = 0.07', 'h = 0.06', 'h = 0.05', 'h = 0.04', 'h = 0.03', 'h = 0.02', 'h = 0.01', 'Location', 'SouthEast')
grid minor